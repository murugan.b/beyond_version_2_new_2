const db = require('../models/index')
const CronJob = require('cron').CronJob
const Free_trial = db.free_trial
exports.scheduler = async() => {
    try {
        let data = await Free_trial.findAll({
            where: {status:1}
        })
        if(data.length>0) {
            for(let i=0;i<data.length;i++) {
                console.log("data.................  "+data[i].cron_time)
                let job = new CronJob(data[i].cron_time,async function() {
                    var fdata = await Free_trial.update(
                        {
                          status: 2,
                          global_key: 3
                        },
                        { where: {id:data[i].id}})
                        this.stop()
                    })
                    job.start()
            }
        }
        
        
    }catch(error) {
        console.log(error)
    }
}

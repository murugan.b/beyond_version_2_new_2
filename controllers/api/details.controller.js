const { v5 } = require('uuid');
const db = require('../../models/index');
const { Op } = require('sequelize');
var Sequelize = require('sequelize');
require('dotenv').config();
var AWS = require('aws-sdk');
const University = db.university;
const College = db.college;
const Degree = db.degree;
const Semester = db.semester;
const Subject = db.subject;
const Module = db.module;
const Deep_dive = db.deep_dive;
const Quick_prep_deep_dive_link = db.quick_prep_deep_dive_link;
const Quick_prep = db.quick_prep;
const Quick_prep_resource = db.quick_prep_resource;
const Quick_prep_index = db.quick_prep_index;
const Customer_support = db.customer_support;
const Purchased_subject = db.purchased_subject;
const Payment_transaction = db.payment_transaction;
const Payment_failed = db.payment_faileds;
const Voucher = db.voucher;
const Customer = db.customers;

const Deep_dive_question_details = db.deep_dive_question_details;
const Deep_dive_question = db.deep_dive_question;
//september-2021
const Free_trial = db.free_trial;
const ReferralCodeHistory = db.referral_code_history;
//end september 2021

//Jun 10

const Voucher_user = db.voucher_user;

//Jun 10

//Get Modules Details List  on Deep Dives //May21
exports.deep_dive_list = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		// $free_trial_status = a[0].free_trial;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let module = await Module.findAll({
				where: {
					subject_id: req.body.subject_id,
					active: 1,
				},
			});
			let length = module.length;
			if (length > 0) {
				if ($free_trial_status == 1) {
					res.json({
						status: true,
						data: {
							module,
							subject_paid: true,
						},
					});
				} else {
					let pur_course = await Purchased_subject.findAll({
						where: {
							subject_id: req.body.subject_id,
							user_id: req.body.user_id,
						},
					});
					let check = pur_course.length;
					if (check > 0) {
						res.json({
							status: true,
							data: {
								module,
								subject_paid: true,
							},
						});
					} else {
						res.json({
							status: true,
							data: {
								module,
								subject_paid: false,
							},
						});
					}
				}
			} else {
				res.json({
					status: false,
					message: 'data not found',
				});
			}
		}
	}
};

//Get Overall Question Details on Modules List in Deep Dives deep_dive_question_list
//May 21
exports.deep_dive_question_list = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		// $free_trial_status = a[0].free_trial;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			if ($free_trial_status == 1 || $free_trial_status == 2) {
				$fStatus = true;
			} else {
				$fStatus = false;
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let idss = await Deep_dive.findAll({
				where: { module_id: req.body.module_id },
			});
			if (idss.length > 0) {
				let question = await db.deep_dive_question.findAll({
					where: { module_id: req.body.module_id, active: 1 },
				});
				if (question.length > 0) {
					let module = await db.module.findAll({
						where: { id: req.body.module_id },
					});
					if (module.length > 0) {
						if ($free_trial_status == 1) {
							res.json({
								status: true,
								data: {
									idss,
									question,
									subject_paid: true,
								},
							});
						} else {
							let subject_id = module[0].subject_id;
							let pur_course = await Purchased_subject.findAll({
								where: {
									subject_id: subject_id,
									user_id: req.body.user_id,
								},
							});
							let check = pur_course.length;
							if (check > 0) {
								res.json({
									status: true,
									data: {
										idss,
										question,
										subject_paid: true,
										free_trial_status: $fStatus,
									},
								});
							} else {
								res.json({
									status: true,
									data: {
										idss,
										question,
										subject_paid: false,
										free_trial_status: $fStatus,
									},
								});
							}
						}
					} else {
						res.json({
							status: false,
							message: 'data not found',
						});
					}
				} else {
					res.json({
						status: false,
						message: 'data not found',
					});
				}
			} else {
				res.json({
					status: false,
					message: 'data not found',
				});
			}
		}
	}
};

//Get Sub Question Details on Deep Dives Question List
exports.deep_dive_question_list_fulldetails = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let quick_prep_resources = await Quick_prep_resource.findAll({
				where: {
					quick_prep_id: req.body.quick_prep_id,
					active: 1,
				},
			});
			let length = quick_prep_resources.length;
			if (length > 0) {
				res.json({
					status: true,
					data: {
						quick_prep_resources,
					},
				});
			} else {
				res.json({
					status: false,
					message: 'data not found',
				});
			}
		}
	}
};
//End Deep Dive
//Start Quick Prep
//get_quick_prep
exports.get_quick_prep = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let overall = Subject.findAll({
				attributes: ['name', 'id'],
				include: [
					{
						model: Quick_prep_index,
						where: {
							id: db.Sequelize.col('quick_prep_index.subject_id'),
							subject_id: req.body.subject_id,
							active: 1,
						},
						attributes: [
							'id',
							'exam_year',
							'exam_month',
							'listing_order',
							'subject_id',
							'download_link',
							'locked',
							'active',
							'createdAt',
							'updatedAt',
						],
					},
				],
			}).then((users) => {
				let len = users.length;
				if (len > 0) {
					res.json({
						status: true,
						data: {
							users,
						},
					});
				} else {
					res.json({
						status: false,
						message: 'data not found',
					});
				}
			});
		}
	}
};
//May 21
exports.get_quick_prep_question_list = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		// $free_trial_status = a[0].free_trial;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let qpi = await db.quick_prep_index.findAll({
				where: { id: req.body.quickprep_index_id },
			});
			if (qpi.length > 0) {
				let subject_id = qpi[0].subject_id;
				let pur_course = await Purchased_subject.findAll({
					where: {
						subject_id: subject_id,
						user_id: req.body.user_id,
					},
				});
				let check = pur_course.length;
				if (check > 0) {
					let overall = Quick_prep.findAll({
						where: {
							quick_prep_index_id: req.body.quickprep_index_id,
						},
						//attributes: ['id'],
						include: [
							{
								model: Quick_prep_deep_dive_link,
								where: {
									id: db.Sequelize.col('quick_prep.id'),
								},
								attributes: ['module_id'],
								include: [
									{
										model: Module,
										where: {
											id: db.Sequelize.col(
												'quick_prep_deep_dive_link.module_id'
											),
										},
										attributes: ['name'],
									},
								],
							},
						],
					}).then((users) => {
						let len = users.length;

						if ($free_trial_status == 1) {
							res.json({
								status: true,
								data: {
									users,
									subject_paid: true,
								},
							});
						} else {
							if (len > 0) {
								res.json({
									status: true,
									data: {
										users,
										subject_paid: true,
									},
								});
							} else {
								res.json({
									status: false,
									message: 'data not found',
								});
							}
						}
					});
				} else {
					let overall = Quick_prep.findAll({
						where: {
							quick_prep_index_id: req.body.quickprep_index_id,
						},
						//attributes: ['id'],
						include: [
							{
								model: Quick_prep_deep_dive_link,
								where: {
									id: db.Sequelize.col('quick_prep.id'),
								},
								attributes: ['module_id'],
								include: [
									{
										model: Module,
										where: {
											id: db.Sequelize.col(
												'quick_prep_deep_dive_link.module_id'
											),
										},
										attributes: ['name'],
									},
								],
							},
						],
					}).then((users) => {
						if ($free_trial_status == 1) {
							res.json({
								status: true,
								data: {
									users,
									subject_paid: true,
								},
							});
						} else {
							let len = users.length;
							if (len > 0) {
								res.json({
									status: true,
									data: {
										users,
										subject_paid: false,
									},
								});
							} else {
								res.json({
									status: false,
									message: 'data not found',
								});
							}
						}
					});
				}
			} else {
				res.json({
					status: false,
					message: 'data not found',
				});
			}
		}
	}
};
//May20
//Get Sub Question Details on Deep Dives Question List
exports.get_quick_prep_question_list_fulldetails = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		// $free_trial_status = a[0].free_trial;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let quick_prep_resources = await Quick_prep_resource.findAll({
				where: {
					quick_prep_id: req.body.quick_prep_id,
					active: 1,
				},
			});
			let length = quick_prep_resources.length;
			if (length > 0) {
				let qp = await db.quick_prep.findAll({
					where: { id: req.body.quick_prep_id },
				});
				if (qp.length > 0) {
					let qpid = qp[0].quick_prep_index_id;
					let qpi = await db.quick_prep_index.findAll({
						where: { id: qpid },
					});
					if (qpi.length > 0) {
						let subject_id = qpi[0].subject_id;

						let subject_details = await Subject.findAll({
							where: { id: subject_id },
						});
						let sub_price = subject_details[0].price;

						let pur_course = await Purchased_subject.findAll({
							where: {
								subject_id: subject_id,
								user_id: req.body.user_id,
							},
						});
						let check = pur_course.length;

						if ($free_trial_status == 1) {
							res.json({
								status: true,
								data: {
									quick_prep_resources,
									subject_paid: true,
									subject_id: subject_id,
									subject_price: sub_price,
									free_trial_status: $free_trial_status,
								},
							});
						} else {
							if (check > 0) {
								res.json({
									status: true,
									data: {
										quick_prep_resources,
										subject_paid: true,
										subject_id: subject_id,
										subject_price: sub_price,
										free_trial_status: $free_trial_status,
									},
								});
							} else {
								res.json({
									status: true,
									data: {
										quick_prep_resources,
										subject_paid: false,
										subject_id: subject_id,
										subject_price: sub_price,
										free_trial_status: $free_trial_status,
									},
								});
							}
						}
					} else {
						res.json({
							status: false,
							message: 'data not found',
						});
					}
				} else {
					res.json({
						status: false,
						message: 'data not found',
					});
				}
			} else {
				res.json({
					status: false,
					message: 'data not found',
				});
			}
		}
	}
};
//End  Quick Prep
//Customer Support
const mailgun = require('mailgun-js');

exports.customer_support = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let customer_id = req.body.customer_id;
			let message = req.body.message;
			let customer_support = await Customer_support.findAll({
				where: {
					customer_id: customer_id,
					message: message,
				},
			});
			let length = customer_support.length;
			if (length > 0) {
				return res.json({
					status: false,
					message: 'Already Added Details',
				});
			} else {
				let b = await db.customers.findAll({
					where: { id: req.body.customer_id },
				});
				$se_id = b[0].email;

				const DOMAIN = 'beyond-theory.com';
				const mg = mailgun({
					apiKey: 'cca24fd139dba21f84bac0ed88e8fcc7-e5e67e3e-875783d0',
					domain: DOMAIN,
				});
				const data = {
					//  from: "Beyond Theory Admin <admin@beyond-theory.com>",
					from: $se_id,
					to: 'admin@beyond-theory.com',
					subject: 'Customer Support Message - Beyond Theory',
					text: req.body.message,
				};
				mg.messages().send(data, function (error, body) {
					console.log(body);
				});

				db.customer_support.create({
					customer_id: customer_id,
					message: message,
				});
				return res.json({
					status: true,
					message: 'Customer Support Message  added successfully',
				});
			}
		}
	}
};
//Purchase Course Register
exports.purchase_course_register = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let user_id = req.body.user_id;
			let subject_id = req.body.subject_id;
			let payment_transaction_id = req.body.payment_transaction_id;
			let purchased_subject = await Purchased_subject.findAll({
				where: {
					user_id: user_id,
					subject_id: subject_id,
				},
			});
			let length = purchased_subject.length;
			if (length > 0) {
				return res.json({
					status: false,
					message: 'Already Added',
				});
			} else {
				Purchased_subject.create({
					user_id: user_id,
					subject_id: subject_id,
					payment_transaction_id: payment_transaction_id,
				});
				//Divya 08/09/2021
				if (req.body.reward_deduction) {
					let balance_amount =
						a[0].balance_amount - req.body.reward_deduction;
					if (balance_amount < 0) {
						return res.json({
							status: false,
							message: 'No sufficient reward balance',
						});
					} else {
						await Customer.update(
							{ balance_amount: balance_amount },
							{ where: { id: a[0].id } }
						);
					}
				}
				if (req.body.vc == 'cc') {
					let b = await Voucher_user.findAll({
						where: { user_id: user_id, code: req.body.code },
					});

					let vulength = b.length;
					//console.log(vulength);console.log("Murugan1111");
					if (vulength == 0) {
						let b = await Voucher.findAll({
							where: { active: 1, code: req.body.code },
						});
						$se_id = b[0].remaining_count - 1;

						var z = Voucher.update(
							{ remaining_count: $se_id },
							{ where: { code: req.body.code } }
						);
						let otp_register = db.voucher_user.create({
							user_id: user_id,
							code: req.body.code,
						});
					}
					//  Voucher_user.create({
					//   user_id: user_id,
					//   code:req.body.code
					//   });
				} else if (req.body.vc == 'rc') {
					let r = await ReferralCodeHistory.findAll({
						where: {
							referral_code: req.body.code,
							used_by: a[0].id,
						},
					});
					if (r.length == 0) {
						let rc = await Customer.findOne({
							where: { referral_code: req.body.code },
						});
						if (rc) {
							let date = new Date();
							let total =
								rc.total_amount + req.body.referral_amount;
							let balance =
								rc.balance_amount + req.body.referral_amount;
							await ReferralCodeHistory.create({
								customer_id: rc.id,
								date: date,
								amount: req.body.referral_amount,
								referral_code: req.body.code,
								used_by: a[0].id,
							});
							await Customer.update(
								{
									total_amount: total,
									balance_amount: balance,
								},
								{ where: { id: rc.id } }
							);
						}
					}
				}
				//End Divya

				// let b = await Voucher_user.findAll({
				// where: {user_id: user_id,code:req.body.code},});

				// let vulength= b.length;
				// //console.log(vulength);console.log("Murugan1111");
				// if(vulength == 0)
				// {

				//         let b = await Voucher.findAll({
				//         where: {  active: 1, code:req.body.code},});
				//         $se_id=b[0].remaining_count-1;

				//         var z = Voucher.update(
				//           { remaining_count:  $se_id },
				//         { where: { code:req.body.code } }
				//         );
				//         let otp_register =  db.voucher_user.create({
				//         user_id: user_id,
				//         code:req.body.code,
				//         });
				// }
				//                         //  Voucher_user.create({
				//                         //   user_id: user_id,
				//                         //   code:req.body.code
				//                         //   });

				return res.json({
					status: true,
					message: 'Course Purchase  added successfully',
				});
			}
		}
	}
};

//Payment Transaction
exports.payment_transaction = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let transaction_id = req.body.transaction_id;
			let user_id = req.body.user_id;
			let voucher_id = req.body.voucher_id;
			let transaction_meta = req.body.transaction_meta;
			let payment_amount = req.body.payment_amount;
			let payment_transaction = await Payment_transaction.findAll({
				where: {
					user_id: user_id,
					voucher_id: voucher_id,
					transaction_id: transaction_id,
				},
			});
			let length = payment_transaction.length;
			if (length > 0) {
				return res.json({
					status: false,
					message: 'Already Added',
				});
			} else {
				db.payment_transaction.create({
					user_id: user_id,
					voucher_id: voucher_id,
					transaction_id: transaction_id,
					transaction_meta: transaction_meta,
					payment_amount: payment_amount,
				});
				return res.json({
					status: true,
					message: 'Payment Transaction   added successfully',
				});
			}
		}
	}
};

//Payment Transaction Failed
exports.payment_failed = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let user_id = req.body.user_id;
			let reason = req.body.reason;
			let transaction_id = req.body.transaction_id;
			let payment_amount = req.body.payment_amount;
			let status_payment = req.body.status_payment;
			let payment_transaction = await Payment_failed.findAll({
				where: {
					user_id: user_id,
					transaction_id: transaction_id,
				},
			});
			let length = payment_transaction.length;
			if (length > 0) {
				return res.json({
					status: false,
					message: 'Already Added',
				});
			} else {
				db.payment_faileds.create({
					user_id: user_id,
					reason: reason,
					transaction_id: transaction_id,
					reason: reason,
					payment_amount: payment_amount,
					status_payment: status_payment,
				});
				return res.json({
					status: true,
					message: 'Payment Response   added successfully',
				});
			}
		}
	}
};

//Subject Payment  subject_payment (May 21)
//Subject Payment  subject_payment (May 21)
exports.subject_payment = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let b = await Customer.findAll({
				where: { id: req.body.user_id },
			});
			if (b.length > 0) {
				$degree_id_find = b[0].degree_id;
				$se_id = b[0].semester_id;
				let overall = Subject.findAll({
					where: {
						semester_id: {
							[Op.eq]: $se_id,
						},
						active: 1,
						degree_id: $degree_id_find,
					},
				}).then((users) => {
					console.log(users);
					let len = users.length;
					if (len > 0) {
						res.json({
							status: true,
							data: {
								users,
							},
						});
					} else {
						res.json({
							status: false,
							message: 'data  not found',
						});
					}
				});
			} else {
				res.json({
					status: false,
					message: 'data  not found',
				});
			}
		}
	}
};

//Get register_count_Details
exports.register_count_Details = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let quick_prep_id = req.body.quick_prep_id;
			let quick_prep_resource_id = req.body.question_id;
			let data = await Quick_prep_resource.findAll({
				where: {
					id: quick_prep_resource_id,
				},
			});
			let count_details = await db.register_count_details.findAll({
				where: {
					quick_prep_id: quick_prep_id,
					quick_prep_resource_id: quick_prep_resource_id,
				},
			});
			var len = count_details.length;
			if (len < 1) {
				db.register_count_details.create({
					quick_prep_id: quick_prep_id,
					quick_prep_resource_id: quick_prep_resource_id,
				});
			}
			return res.json({
				status: true,
				data,
			});
		}
	}
};
//deepdive_to_quickprep //May 22
exports.deepdive_to_quickprep = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		// $free_trial_status = a[0].free_trial;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let a = await Deep_dive_question.findAll({
				where: { id: req.body.question_id },
			});
			if (a.length > 0) {
				let module_id = a[0].module_id;
				let module_topic_id = a[0].id;
				let b = await Module.findAll({
					where: { id: module_id },
				});
				if (b.length > 0) {
					let module_name = b[0].name;
					let subject_id = b[0].subject_id;
					let idss = await Quick_prep_deep_dive_link.findAll({
						where: { module_id: module_id },
					});
					if (idss.length > 0) {
						let quick_prep_id = idss[0].quick_prep_id;
						let idss2 = await Quick_prep.findAll({
							where: { module_topic_id: module_topic_id },
						});

						let pur_course = await Purchased_subject.findAll({
							where: {
								subject_id: subject_id,
								user_id: req.body.user_id,
							},
						});
						let check = pur_course.length;

						if ($free_trial_status == 1) {
							res.json({
								status: true,
								data: {
									idss2,
									module_name: module_name,
									subject_paid: true,
								},
							});
						} else {
							if (check > 0) {
								res.json({
									status: true,
									data: {
										idss2,
										module_name: module_name,
										subject_paid: true,
									},
								});
							} else {
								res.json({
									status: true,
									data: {
										idss2,
										module_name: module_name,
										subject_paid: false,
									},
								});
							}
						}
					} else {
						res.json({
							status: false,
							message: 'data not found',
						});
					}
				} else {
					res.json({
						status: false,
						message: 'data not found',
					});
				}
			} else {
				res.json({
					status: false,
					message: 'data not found',
				});
			}
		}
	}
};
//quickprep_deepdive Quick to Deep Dive // May 21
exports.quickprep_deepdive = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		// $free_trial_status = a[0].free_trial;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let cv = await Quick_prep_deep_dive_link.findAll({
				where: { quick_prep_id: req.body.quick_prep_id },
			});
			if (cv.length > 0) {
				let module_id = cv[0].module_id;
				let idss = await Deep_dive.findAll({
					where: { module_id: module_id },
				});
				let question = await db.deep_dive_question.findAll({
					where: { module_id: module_id, active: 1 },
				});
				let module = await db.module.findAll({
					where: { id: module_id },
				});
				if (module.length > 0) {
					let subject_id = module[0].subject_id;
					let pur_course = await Purchased_subject.findAll({
						where: {
							subject_id: subject_id,
							user_id: req.body.user_id,
						},
					});
					let check = pur_course.length;

					if ($free_trial_status == 1) {
						res.json({
							status: true,
							data: {
								idss,
								question,
								subject_paid: true,
							},
						});
					} else {
						if (check > 0) {
							res.json({
								status: true,
								data: {
									idss,
									question,
									subject_paid: true,
								},
							});
						} else {
							res.json({
								status: true,
								data: {
									idss,
									question,
									subject_paid: false,
								},
							});
						}
					}
				} else {
					res.json({
						status: false,
						message: 'data not found',
					});
				}
			} else {
				res.json({
					status: false,
					message: 'data not found',
				});
			}
		}
	}
};
//deep_dive_question_details //May 21
exports.deep_dive_question_details = async (req, res, next) => {
	try {
		let a = await Customer.findAll({
			where: { api_token: req.body.token },
		});
		if (a.length == 0) {
			res.json({
				status: false,
				message: 'Token Not Found',
			});
		} else {
			$active_status = a[0].active;
			// $free_trial_status = a[0].free_trial;
			if ($active_status == 0 || $active_status == false) {
				return res.json({
					status: false,
					message:
						'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
				});
			} else {
				console.log(a[0] + 'A');
				let fData = await Free_trial.findOne({
					where: {
						[Op.and]: [
							{ semester_id: a[0].semester_id },
							{ customer_id: a[0].id },
						],
					},
				});
				console.log(fData);
				console.log(fData === null);
				// console.log("Free trial status of customer........"+fData.customer_id);
				if (fData === null) {
					$free_trial_status = '';
				} else {
					$free_trial_status = fData.status;
				}
				// var  date = new Date(); var hours = 0;
				// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
				// let hours2 = azx.getHours();
				// let minutes = azx.getMinutes();
				// let date22 = ("0" + azx.getDate()).slice(-2)
				// var mon = date.getMonth()+ 1;
				// var customer_month = a[0].expire_month;
				// var customer_date = a[0].expire_date;
				// var customer_hrs = a[0].expire_hrs;
				// var customer_mins = a[0].expire_mins;
				// if(customer_month >= mon)
				// {
				//   if(customer_date >= date22)
				//     {
				//       if(customer_hrs >= hours2)
				//         {
				//           if(customer_mins >= minutes)
				//             {
				//                 var init_v = 2;
				//                 var insert_data = db.customers.update(
				//                   {
				//                     free_trial:init_v
				//                   },
				//                   {  where: { api_token: req.body.token } }
				//                 );

				//             }
				//         }
				//     }
				// }

				let idss = await Deep_dive_question_details.findAll({
					where: { question_id: req.body.question_id },
				});
				console.log(
					'deep dive question details..........' + idss[0].id
				);
				let len = idss.length;
				if (len > 0) {
					let ques = await Deep_dive_question.findAll({
						where: { id: req.body.question_id },
					});
					if (ques.length > 0) {
						console.log(
							'deep dive questions...................' +
								ques[0].id
						);
						let module_id = ques[0].module_id;
						let module = await db.module.findAll({
							where: { id: module_id },
						});
						if (module.length > 0) {
							console.log(
								'Modules......................' + module[0].id
							);
							let subject_id = module[0].subject_id;

							let subject_details = await Subject.findAll({
								where: { id: subject_id },
							});
							console.log(
								'Subject...................' +
									subject_details[0].id
							);
							let sub_price = subject_details[0].price;

							console.log('Purchased subjects.................');
							let pur_course = await Purchased_subject.findAll({
								where: {
									subject_id: subject_id,
									user_id: req.body.user_id,
								},
							});
							let check = pur_course.length;

							if ($free_trial_status == 1) {
								res.json({
									status: true,
									data: {
										idss,
										subject_paid: true,
										subject_id: subject_id,
										subject_price: sub_price,
										free_trial_status: $free_trial_status,
									},
								});
							} else {
								if (check > 0) {
									res.json({
										status: true,
										data: {
											idss,
											subject_paid: true,
											subject_id: subject_id,
											subject_price: sub_price,
											free_trial_status:
												$free_trial_status,
										},
									});
								} else {
									res.json({
										status: true,
										data: {
											idss,
											subject_paid: false,
											subject_id: subject_id,
											subject_price: sub_price,
											free_trial_status:
												$free_trial_status,
										},
									});
								}
							}
						} else {
							res.json({
								status: false,
								message: 'data not found',
							});
						}
					} else {
						res.json({
							status: false,
							message: 'data not found',
						});
					}
				} else {
					res.json({
						status: false,
						message: 'data not found',
					});
				}
			}
		}
	} catch (error) {
		console.log(error);
	}
};

exports.deep_dive_module_list = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		//  $free_trial_status = a[0].free_trial;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let idss = await Deep_dive.findAll({
				where: { module_id: req.body.module_id },
			});
			if (idss.length > 0) {
				let question = await db.deep_dive_question.findAll({
					where: { module_id: req.body.module_id, active: 1 },
				});
				if (question.length > 0) {
					let module = await db.module.findAll({
						where: { id: req.body.module_id },
					});
					if (module.length > 0) {
						let subject_id = module[0].subject_id;
						let pur_course = await Purchased_subject.findAll({
							where: {
								subject_id: subject_id,
								user_id: req.body.user_id,
							},
						});
						let check = pur_course.length;

						if ($free_trial_status == 1) {
							res.json({
								status: true,
								data: {
									idss,
									question,
									subject_paid: true,
								},
							});
						} else {
							if (check > 0) {
								res.json({
									status: true,
									data: {
										idss,
										question,
										subject_paid: true,
									},
								});
							} else {
								res.json({
									status: true,
									data: {
										idss,
										question,
										subject_paid: false,
									},
								});
							}
						}
					} else {
						res.json({
							status: false,
							message: 'data not found',
						});
					}
				} else {
					res.json({
						status: false,
						message: 'data not found',
					});
				}
			} else {
				res.json({
					status: false,
					message: 'data not found',
				});
			}
		}
	}
};

//Purchase History
exports.purchase_history = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let overall = Subject.findAll({
				include: [
					{
						model: Purchased_subject,
						where: {
							subject_id: db.Sequelize.col('subject.id'),
							user_id: req.body.user_id,
						},
					},
				],
			}).then((users) => {
				let len = users.length;
				if (len > 0) {
					res.json({
						status: true,
						data: {
							users,
						},
					});
				} else {
					res.json({
						status: false,
						message: 'data not found',
					});
				}
			});
		}
	}
};

//May 22
exports.deep_to_quick_first = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		// $free_trial_status = a[0].free_trial;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			let fData = await Free_trial.findOne({
				where: {
					[Op.and]: [
						{ semester_id: a[0].semester_id },
						{ customer_id: a[0].id },
					],
				},
			});
			if (fData) {
				$free_trial_status = fData.status;
			} else {
				$free_trial_status = '';
			}

			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			let module = await db.module.findAll({
				where: { id: req.body.module_id },
			});
			if (module.length > 0) {
				let subject_id = module[0].subject_id;
				let pur_course = await Purchased_subject.findAll({
					where: {
						subject_id: subject_id,
						user_id: req.body.user_id,
					},
				});
				let check = pur_course.length;
				if (check > 0) {
					$status = true;
				} else {
					$status = false;
				}
			} else {
				$status = false;
			}

			let overall = Quick_prep_deep_dive_link.findAll({
				where: { module_id: req.body.module_id },
				attributes: ['quick_prep_id', 'module_id'],
				include: [
					{
						model: Quick_prep,
						where: {
							id: db.Sequelize.col(
								'quick_prep_deep_dive_link.quick_prep_id'
							),
						},
					},
				],
			}).then((users) => {
				let len = users.length;

				if ($free_trial_status == 1) {
					res.json({
						status: true,
						data: {
							users,
							subject_paid: true,
						},
					});
				} else {
					if (len > 0) {
						res.json({
							status: true,
							data: {
								users,
								subject_paid: $status,
							},
						});
					} else {
						res.json({
							status: false,
							message: 'data not found',
						});
					}
				}
			});
		}
	}
};

//Jun 11

//Coupons Code Verify coupons_user_verify

exports.coupons_user_verify = async (req, res, next) => {
	let a = await Customer.findAll({
		where: { api_token: req.body.token },
	});
	if (a.length == 0) {
		res.json({
			status: false,
			message: 'Token Not Found',
		});
	} else {
		$active_status = a[0].active;
		if ($active_status == 0 || $active_status == false) {
			return res.json({
				status: false,
				message:
					'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
			});
		} else {
			// var  date = new Date(); var hours = 0;
			// var azx = new Date(new Date(date).setHours(date.getHours() + hours));
			// let hours2 = azx.getHours();
			// let minutes = azx.getMinutes();
			// let date22 = ("0" + azx.getDate()).slice(-2)
			// var mon = date.getMonth()+ 1;
			// var customer_month = a[0].expire_month;
			// var customer_date = a[0].expire_date;
			// var customer_hrs = a[0].expire_hrs;
			// var customer_mins = a[0].expire_mins;
			// if(customer_month >= mon)
			// {
			//   if(customer_date >= date22)
			//     {
			//       if(customer_hrs >= hours2)
			//         {
			//           if(customer_mins >= minutes)
			//             {
			//                 var init_v = 2;
			//                 var insert_data = db.customers.update(
			//                   {
			//                     free_trial:init_v
			//                   },
			//                   {  where: { api_token: req.body.token } }
			//                 );

			//             }
			//         }
			//     }
			// }

			var coupon_checking = await Voucher.findAll({
				where: {
					code: req.body.code,
				},
			});
			if (coupon_checking.length > 0) {
				let coupon_count = await db.voucher_user.findAll({
					where: {
						user_id: req.body.user_id,
						code: req.body.code,
					},
				});

				if (coupon_count.length > 0) {
					res.json({
						status: false,
						message: 'Already Used  Coupons ',
					});
				} else {
					var coupon = await Voucher.findAll({
						where: {
							code: req.body.code,
						},
					});
					let active_id = coupon[0].active;

					let rcount = coupon[0].remaining_count;

					if (active_id == true) {
						if (rcount == 0) {
							res.json({
								status: true,
								coupon: null,
								message: 'Coupon Are Expired',
							});
						} else {
							res.json({
								status: true,
								coupon,
							});
						}
					} else {
						res.json({
							status: true,
							message: 'Coupon Are Inactive',
							coupon: null,
						});
					}
				}
			} else {
				res.json({
					status: false,
					message: 'Coupon Not Found',
					coupon: null,
				});
			}
		}
	}
};

//Jun 11

//July 9

exports.version_checking = async (req, res, next) => {
	let version_count = await db.version.findAll({
		where: { name: req.body.version },
	});
	if (version_count.length > 0) {
		res.json({ status: true, message: 'Please Login to Access ' });
	} else {
		res.json({ status: false, message: 'Please update New Version ' });
	}
};

//July 9

//Author: Divya, Date:06/09/2021
// List coupons
exports.showCoupons = async (req, res) => {
	try {
		let a = await Customer.findAll({
			where: { api_token: req.body.token },
		});
		if (a.length == 0) {
			return res.json({
				status: false,
				message: 'Token Not Found',
			});
		} else {
			$active_status = a[0].active;
			if ($active_status == 0 || $active_status == false) {
				return res.json({
					status: false,
					message:
						'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>',
				});
			} else {
				let c = await Voucher.findAll({
					where: { show_in_app: 1 },
				});
				if (c.length == 0) {
					return res.json({
						status: false,
						message: 'No coupons to show',
					});
				} else {
					return res.json({
						status: true,
						message: 'Coupns list',
						coupons: c,
					});
				}
			}
		}
	} catch (error) {
		console.log(error);
		return res.json({
			status: false,
			message: 'Sorry...Something went wrong',
		});
	}
};

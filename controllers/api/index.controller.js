const { v5 } = require("uuid");
const db = require("../../models/index");
const { Op } = require("sequelize");

const University = db.university;
const College = db.college;
const Degree = db.degree;
const Semester = db.semester;
const Subject = db.subject;
const Module = db.module;
const Deep_dive = db.deep_dive;


exports.page = async (req, res, next) => {
    let  overall  =  University.findAll({
      where: { active:1},        
       }).then(users => {
         let len =users.length;
        if(len>0)
        {
          res.json({
            status: true,
            data: {
                users,
              
            },
          });
        }
        else
        {
          res.json({
            status: false,
            message:'data not found',
          });
        }
        });
};


exports.college_details = async (req, res, next) => {
  let  totalcollege  =  College.findAll({
    where: { university_id: req.body.university_id , active:1},
  }).then(users => {
    let len =users.length;
    if(len>0)
    {
      res.json({
        status: true,
        data: {
            users,
        },
      });
    }
    else
    {
      res.json({
        status: false,
        message:'data not found',
      });

    }
    });
};

exports.degree_details = async (req, res, next) => {
  let  totaldegree  =  Degree.findAll({
    where: { university_id: req.body.university_id , active:1 },
  }).then(users => {
    let len =users.length;
    if(len>0)
    {
      res.json({
        status: true,
        data: {
            users,
        },
      });
    }
    else
    {
      res.json({
        status: false,
        message:'data not found',
      });
    }  
  });
};

exports.semester_details = async (req, res, next) => {
  let  totalsemester  =  Semester.findAll({
    where: { degree_id: req.body.degree_id , active:1 },
  }).then(users => {
    let len =users.length;
    if(len>0)
    {
      res.json({
        status: true,
        data: {
            users,
        },
      });
    }
    else
    {
      res.json({
        status: false,
        message:'data not found',
      });
    }   
    });
};



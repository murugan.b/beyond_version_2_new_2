const { v5 } = require("uuid");
const db = require("../../models/index");
const { Op } = require("sequelize");
const CronJob = require('cron').CronJob
const University = db.university;
const College = db.college;
const Degree = db.degree;
const Semester = db.semester;
const Subject = db.subject;
const Module = db.module;
const Deep_dive = db.deep_dive;
const Purchased_subject = db.purchased_subject;
const Customer = db.customers;
const Free_trial = db.free_trial
const Global_settings = db.global_settings

exports.page = async (req, res, next) => {
  let a = await Customer.findAll({
    where: { api_token: req.body.token},
  });
    if(a.length == 0)
        {
            res.json({
            status: false,
            message:'Token Not Found',
                    });
        }
        else
            {
              $active_status = a[0].active;
              if($active_status == 0 ||  $active_status == false)
                 {
                 
                   return res.json({
                     status: false,
                     message: "Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>",
                               });
                 }
                 else
                 {


                        //   var  date = new Date(); var hours = 0;
                        //   var azx = new Date(new Date(date).setHours(date.getHours() + hours));
                        //   let hours2 = azx.getHours();
                        // // console.log(hours2);
                        //   let minutes = azx.getMinutes();
                        // // console.log(minutes);
                        //   let date22 = ("0" + azx.getDate()).slice(-2)
                        //   //console.log(date22);
                        //   var mon = date.getMonth()+ 1;
                        // // console.log(mon);

                        // var customer_month = a[0].expire_month;
                        // var customer_date = a[0].expire_date;
                        // var customer_hrs = a[0].expire_hrs;
                        // var customer_mins = a[0].expire_mins;

                        // if(customer_month >= mon)
                        // {
                        //     if(customer_date >= date22)
                        //       {
                        //         if(customer_hrs >= hours2)
                        //           {
                        //             if(customer_mins >= minutes)
                        //               {
                        //                   var init_v = 2;
                        //                   var insert_data = db.customers.update(
                        //                     {  
                        //                       free_trial:init_v
                        //                     },  
                        //                     {  where: { api_token: req.body.token } }
                        //                   );

                        //               }
                        //           }
                        //       }
                        // }




                      let subject_degree = await Semester.findAll({
                        where: { id: req.body.semester_id},
                        });
                        $degree_id_find = subject_degree[0].degree_id;
                        let  overall  =  Subject.findAll({
                          where: {
                            semester_id: {
                            [Op.lte]: req.body.semester_id
                            }, active:1,degree_id:$degree_id_find
                            },
                            }).then(users => {
                              let len =users.length;
                              if(len>0)
                              {
                                res.json({
                                  status: true,
                                  data: {
                                      users,
                                  },
                                });
                              }
                              else
                              {
                                res.json({
                                  status: false,
                                  message:'data not found',
                                        });
                              }   
                                        });
                 }
              
            }
};


exports.free_trial_activated_check = async (req, res, next) => {

  let a = await Customer.findAll({
    where: { api_token: req.body.token},
  });
    if(a.length == 0)
        {
            res.json({
            status: false,
            message:'Token Not Found',
                    });
        }
        else
            {
              $active_status = a[0].active;
              if($active_status == 0 ||  $active_status == false)
                 {
                 
                   return res.json({
                     status: false,
                     message: "Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>",
                               });
                 }
                 else
                 {

                        var free_trail_found 
                        //
                        let trialData = await Free_trial.findOne({
                          where: {
                            [Op.and]: [{customer_id:a[0].id}, {semester_id:a[0].semester_id}]
                          }
                        })
                        if (!trialData) {
                          free_trail_found = 0
                        }
                        else {
                          free_trial_found = trialData.status
                        }
                        //
                        if(free_trail_found == 0)
                        {
                              return res.json({
                                status: true,
                                message: "Not Activated Free Trial ",
                                });
                        }
                        else if(free_trail_found == 1)
                          {
                            
                              return res.json({
                                status: true,
                                message: "Already Free Trial Activated",
                                });
                            
                          }
                          else if(free_trail_found == 2)
                          {
                                return res.json({
                                status: true,
                                message: "Free Trial Expired",
                                              });
                            
                          }
                     
                     

              
                 }
              
            }
  
};






//Edit start 26/08/2021 - 24 hr Free trial activation
exports.free_trial_activated = async (req, res, next) => {
  try{
    let a = await Customer.findAll({
      where: { api_token: req.body.token},
    });
      if(a.length == 0)
          {
              res.json({
              status: false,
              message:'Token Not Found',
                      });
          }
          else
              {
                let semData = await Semester.findOne({
                  where: {id: a[0].semester_id}
                })
                if (semData.enable_free_trial == 0) {
                  let mData = await Global_settings.findOne({
                    where: {id:semData.global_key}
                  })
                  //'Free trial is not available'
                  return res.json({
                    status: false,
                    message: mData.value,
                    free_trial: 0
                  })
                }
                if (a[0].enable_free_trial == 0) {
                  let mData = await Global_settings.findOne({
                    where: {id: a[0].global_key}
                  })
                  //'Free trial is not available'
                  return res.json({
                    status: false,
                    message: mData.value,
                    free_trial: 0
                  })
                }
                $active_status = a[0].active;
                if($active_status == 0 ||  $active_status == false)
                   {
                   
                     return res.json({
                       status: false,
                       message: "Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>",
                                 });
                   }
                   else
                   {
                    let trialData = await Free_trial.findAll({
                      where: {
                        [Op.and]: [{customer_id:a[0].id}, {semester_id:a[0].semester_id}]
                      }
                    })
                    let mData
                    if (semData.enable_free_trial == 1 || a[0].enable_free_trial == 1) {
                      if (trialData.length > 0) {
                        mData = await Global_settings.findOne({
                          where: {id: trialData[0].global_key}
                        })
                        if (trialData[0].status == 2) {
                          //
                          //Return free trial expired msg
                          return res.json({
                            status: false,
                            message: mData.value,
                            free_trial: 2
                          })
                        }
                        else if (trialData[0].status == 1) {
                          return res.json({
                            status: true,
                            message: mData.value,
                            free_trial: 1
                          })
                        }
                      } else {
                        var  date = new Date(); var hours = 24;
                        var azx = new Date(new Date(date).setHours(date.getHours() + hours));
                        let hours2 = azx.getHours();
                        let minutes = azx.getMinutes();
                        let date22 = ("0" + azx.getDate()).slice(-2)
                        let mon = azx.toLocaleString('en-us', { month: 'short' }); /* Jun */
                        var day = azx.getDay()
                        let cronTime = minutes + ' ' + hours2 + ' ' + date22 + ' ' + mon + ' ' + day
                        // let cronTime = 27 + ' ' + 17 + ' ' + 08 + ' ' + mon + ' ' + 3
                        let f = await Free_trial.create({
                          customer_id: a[0].id,
                          semester_id: a[0].semester_id,
                          start_time:date,
                          end_time: azx,
                          cron_time: cronTime,
                          status: 1,
                          global_key: 2
                        })
                        // Cron job
                        let job = new CronJob(cronTime,async function() {
                          var data = await Free_trial.update(
                            {
                              status: 2,
                              global_key: 3
                              
                            },
                            { where: {id:f.id}})
                            this.stop()
                          })
                          job.start()
                          //
                          mData = await Global_settings.findOne({
                          where: {id: f.global_key}
                          })

                          await Free_trial.update(
                            {
                              global_key: 6
                            },
                            {where: {id: f.id}}
                          )

                          return res.json({
                            status: true,
                            message: mData.value,
                            free_trial: 1
                          })
                          
                        }
                    } else {
                      if (semData.enable_free_trial == 0 ) {
                        mData = await Global_settings.findOne({
                          where: {id: semData.global_key}
                        })
                        // Return free trial is not available msg
                        return res.json({
                          status: false,
                          message:mData.value,
                          free_trial: 0
                        })

                      }
                      if (a[0].enable_free_trial == 0) {
                        mData = await Global_settings.findOne({
                          where: {id: a[0].global_key}
                        })
                        // Return free trial is not available msg
                       return res.json({
                         status: false,
                         message:mData.value,
                         free_trial: 0
                       })

                      }
                    }
                      
                
                   }
                
              }
  } catch(error) {
    console.log(error)
    return res.json({
      status: false,
      message:"Sorry, something went wrong"
    })
  }
};
// Edit end 26/08/2021

exports.free_trial_check= async (req, res, next) => {
  let a = await Customer.findAll({
    where: { api_token: req.body.token},
  });
    if(a.length == 0)
        {
            res.json({
            status: false,
            message:'Token Not Found',
                    });
        }
        else
            {
              $active_status = a[0].active;
              if($active_status == 0 ||  $active_status == false)
                 {
                 
                   return res.json({
                     status: false,
                     message: "Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>",
                               });
                 }
                 else
                 {

                     var free_trail_found 
                     //
                     let trialData = await Free_trial.findOne({
                      where: {
                        [Op.and]: [{customer_id:a[0].id}, {semester_id:a[0].semester_id}]
                      }
                    })
                    if (!trialData) {
                      free_trail_found = 0
                    }
                    else {
                      free_trial_found = trialData.status
                    }
                    //
                     if(free_trail_found == 0)
                     {
                          return res.json({
                            status: true,
                            message: "Not Activated Free Trial ",
                            });
                    }
                    else if(free_trail_found == 1)
                      {
                        
                          return res.json({
                            status: true,
                            message: "Already Free Trial Activated",
                            });
                        
                      }
                      else if(free_trail_found == 1)
                      {
                            return res.json({
                            status: true,
                            message: "Free Trial Expired",
                                          });
                        
                      }
                      else
                        {
                          return res.json({
                            status: false,
                            message: "Free Trial Expired",
                                          });
                        
                        }
                     

              
                 }
              
            }
};

exports.switch_semester= async (req, res, next) => {
  let a = await Customer.findAll({
    where: { api_token: req.body.token},
  });
    if(a.length == 0)
        {
            res.json({
            status: false,
            message:'Token Not Found',
                    });
        }
        else
            {
              $active_status = a[0].active;
              if($active_status == 0 ||  $active_status == false)
                 {
                 
                   return res.json({
                     status: false,
                     message: "Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>",
                               });
                 }
                 else
                 {

                      let  totalsemester  =  Semester.findAll({
                        where: { degree_id: req.body.degree_id , active:1 },
                      }).then(semester => {
                        let len =semester.length;
                        if(len>0)
                        {
                          res.json({
                            status: true,
                            data: {
                              semester,
                            },
                          });
                        }
                        else
                        {
                          res.json({
                            status: false,
                            message:'data not found',
                          });
                        }   
                        });


                 }
              
            }
};







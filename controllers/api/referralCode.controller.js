//Date:02/09/2021,Author:Divya
const db = require('../../models/index')

const Customer = db.customers
const ReferralCodeHistory = db.referral_code_history
const Redeem = db.redeem

const mailgun = require('mailgun-js')

//Read referral_code data 
exports.referralCodeDetails = async(req, res) => {
    try {
        let a = await Customer.findAll({
            where: {api_token: req.body.token}
        })
        if (a.length == 0) {
            return res.json({
                status: false,
                message: 'Token Not Found'
            })
        } else {
            $active_status = a[0].active
            if ($active_status == 0 || $active_status == false) {
                return res.json({
                    status: false,
                    message: 'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>'
                })
            } else {
                await ReferralCodeHistory.findAll({
                    where: {customer_id: a[0].id},
                    include: [
                        {
                            model: Customer,
                            where: {id: db.Sequelize.col('referral_code_history.used_by')}
                        }
                    ]
                }).then(rc => {
                    let ar = []
                    rc.forEach(function(element) {
                        const r = {}
                        r.id = element.customer.id
                        r.full_name = element.customer.full_name
                        r.date = element.date
                        r.amount = element.amount
                        r.referral_code = element.referral_code
                        r.colour_shade = '#4E4DE2'
                        ar.push(r)
                    })
                    data = {
                        customer_data: a[0],
                        Referral_data: ar
                    }
                    return res.json({
                        status: true,
                        message: 'Success',
                        data
                    })
                })
                
            }
        }
    } catch(error) {console.log(error)}
}

//Redeem request
exports.redeemRequest = async(req, res) => {
    try{
        let a = await Customer.findAll({
            where: {api_token: req.body.token}
        })
        if (a.length == 0) {
            return res.json({
                status: false,
                message: 'Token Not Found'
            })
        } else {
            $active_status = a[0].active
            if ($active_status ==0 || $active_status == false) {
                return res.json({
                    status: false,
                    message: 'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>'
                })
            } else {
                let redeemAmount = req.body.amount
                if ( redeemAmount <= a[0].balance_amount) {
                    let balance_amount = a[0].balance_amount - redeemAmount
                    await Customer.update(
                        {
                            balance_amount: balance_amount
                        },
                        {
                            where: {id: a[0].id}
                        })
                    let date = new Date()
                    await Redeem.create({
                        customer_id: a[0].id,
                        amount: redeemAmount,
                        date: date
                    })

                    //Mail request
                    let toemail ="admin@beyond-theory.com"
                    const DOMAIN = "beyond-theory.com";
                    const mg = mailgun({apiKey: "cca24fd139dba21f84bac0ed88e8fcc7-e5e67e3e-875783d0", domain: DOMAIN});
                    const data = {
                        from: "Beyond Theory Admin <admin@beyond-theory.com>",
                        to: toemail,
                        subject: "Redeem request",
                        text: 'Redeem request from Customer: ' + a[0].full_name + ', Customer id: '+ a[0].id + ', Amount: ' + redeemAmount
                        };
                    mg.messages().send(data, function (error, body) {
                    console.log(body);
                    });

                    let amount_details = {
                        customer_id: a[0].id,
                        total_amount: a[0].total_amount,
                        balance_amount: balance_amount
                    }
                    return res.json({
                        status: true,
                        message: 'Request sent successfully',
                        amount_details: amount_details
                    })

                } else {
                    return res.json({
                        status: false,
                        message: 'No sufficient balance'
                    })
                }
            }
        }
    } catch(error) {
        console.log(error)
        return res.json({
            status: false,
            message: 'Sorry...,Something went wrong'
        })
    }
}

// Verify referral code
exports.verifyReferralCode = async(req, res) => {
    try {
        let a = await Customer.findAll({
            where: {api_token: req.body.token}
        })
        if (a.length == 0) {
            return res.json({
                status: false,
                message: 'Token Not Found'
            })
        } else {
            $active_status = a[0].active
            if ($active_status == 0 || $active_status == false) {
                return res.json({
                    status: false,
                    message: 'Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>'
                })
            } else {
                if (a[0].referral_code == req.body.referral_code) {
                    return res.json({
                        status: false,
                        message: "Sorry...,You can't use your own referral code"
                    })
                }
                let r = await Customer.findOne({
                    where: {referral_code: req.body.referral_code}
                })
                if (!r) {
                    return res.json({
                        status: false,
                        message: 'Invalid Referral Code'
                    })
                } else {
                    let rh = await ReferralCodeHistory.findOne({
                        where: {
                            referral_code: req.body.referral_code,
                            used_by: a[0].id
                        }
                    })
                    if(rh) {
                        return res.json({
                            status: false,
                            message: 'Sorry...,You have already used the referral code'
                        })
                    } else {
                        return res.json({
                            status: true,
                            message: 'Referral code is valid',
                            referral_code: req.body.referral_code
                        })
                    }
                }
            }
        }
    } catch(error) {console.log(error)}
}
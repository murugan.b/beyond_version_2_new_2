const { v5 } = require("uuid");
const db = require("../../models/index");
const { Op } = require("sequelize");

const Customer = db.customers;

//path /api/login
//method GET
//desc  api login
exports.login = async (req, res, next) => {
  let phone = req.body.phone || 0;
  let email = req.body.email || 0;

  if(phone == '' || email =='')
    {
      return res.json({
        status: false,
        message: "Please Check  Mobile Number  and Email",
                  });
    }
    else

      {
        
              customer = await Customer.findOne({
                where: {
                  delete_status: 0,
                  active: 1,
                  [Op.or]: [{ phone }, { email }],
                },
              });
              if (customer) {
                var ts = Math.round(new Date().getTime() / 1000);
                let api_token = await v5("" + customer.id + ts, res.MY_NAMESPACE);
                customer.api_token = api_token;
                await customer.save();
                res.json({
                  status: true,
                  data: {
                    customer,
                    api_token: api_token,
                  },
                });
              }


      }

 // console.log(phone);

  
};

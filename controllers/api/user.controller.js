const { v5 } = require("uuid");
const db = require("../../models/index");
const { Op } = require("sequelize");
const subjectUpload = require("../../utils/uploads/subject.uploads");

const Customer = db.customers;
const University = db.university;
const College = db.college;
const Degree = db.degree;
const Semester = db.semester;
const Subject = db.subject;
const Module = db.module;
const Deep_dive = db.deep_dive;
const Free_trial = db.free_trial;
const Global_settings = db.global_settings;

// Customer Register Page Details
exports.page = async (req, res, next) => {


  // let imei1=req.body.imei1;
  // let imei2=req.body.imei2;
  let full_name = req.body.full_name;
  let phone = req.body.phone;
  let email = req.body.email;
  let university_id = req.body.university_id;
  let degree_id = req.body.degree_id;
  let semester_id = req.body.semester_id;
  let college_id = req.body.college_id;
  let customer = await Customer.findAll({
      where: {
          phone: phone,
          email:email 
      },
    });
    let length= customer.length;
    if(length>0)
    {
      return res.json({
          status: false,
          message: "Already Added Customer.. Please Login to Access",
        });
    }
    else
    {
      var val = Math.floor(1000 + Math.random() * 9000);
      let referral_code = 'BT_' + full_name + val
      
        db.customers.create({
          full_name: full_name,
          phone:phone,
          email:email,
          image:'',
          university_id:university_id,
          degree_id:degree_id,
          semester_id:semester_id,
          college_id:college_id,
          referral_code: referral_code
        });
      
     
        
      
      return res.json({
          status: true,
          message: "Customer added successfully",
                    });

    }
};






//Customer Get Details
exports.user_details = async (req, res, next) => {
  let a = await Customer.findAll({
    where: { api_token: req.body.token},
  });
  console.log(a)
    if(a.length == 0)
        {
            res.json({
            status: false,
            message:'Token Not Found',
        });
        }
        else
            {
              $active_status = a[0].active;
              let free_trail_found
              if($active_status == 0 ||  $active_status == false)
                 {
                 
                   return res.json({
                     status: false,
                     message: "Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>",
                               });
                 }
                 else
                  {

                            let i = 0
                            let trialData = await Free_trial.findOne({
                              where: {
                                [Op.and]: [{customer_id:a[0].id}, {semester_id:a[0].semester_id}]
                              }
                            })
                            console.log(trialData)
                            if (!trialData) {
                              i = 0
                            }
                            else {
                            i = trialData.status
                            }

                    $college_id = a[0].college_id;


                    let college_details = await College.findAll({
                      where: { id: $college_id },
                      attributes: ['name','id'],
                    });

                   //Divya
                   $rValue = a[0].referral_value
                   let r = await Global_settings.findOne({
                     where: { id: $rValue },
                     attributes:['value']
                   })
                   console.log(r)
                   //Divya End

                   


                    //console.log(college_details);
                    


                    let  overall  =  University.findAll({
                      attributes: ['name','id'],
                      include: [
                        {
                         
                                model: Degree,
                                where: { university_id: db.Sequelize.col('university.id') },
                                attributes: ['name','id'],
                                       include: [
                                           {
                                           model: Semester,
                                           where: { university_id: db.Sequelize.col('university.id')  },
                                          attributes:['name','id'],
                                          include: [
                                            {
                                            model: Customer,
                                            where: { university_id: db.Sequelize.col('university.id')  , id:req.body.user_id},
                                            }
                                          ]
                                       }
                
                                      
                             ]
                        }
                     
                      ] 
                     }).then(users => {
                      let len =users.length;
                      if(len>0)
                      {
                        res.json({
                          status: true,
                          data: {
                              users,
                              college_details,
                              free_trial_status:i,
                              referral_value: r.value
                          },
                        });
                      }
                      else
                      {
                        res.json({
                          status: false,
                          message:'data not found',
                        });
                      }
                      });
                  }
              
            }
};

//Customer Edit Details
exports.edit_user_details = async (req, res, next) => {
  let a = await Customer.findAll({
    where: { api_token: req.body.token},
  });
    if(a.length == 0)
        {
            res.json({
            status: false,
            message:'Token Not Found',
                    });
        }
        else
            {
              $active_status = a[0].active;
              if($active_status == 0 ||  $active_status == false)
                 {
                 
                   return res.json({
                     status: false,
                     message: "Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>",
                               });
                 }
                 else
                  {


                          // var  date = new Date(); var hours = 0;
                          // var azx = new Date(new Date(date).setHours(date.getHours() + hours));
                          // let hours2 = azx.getHours();
                          // let minutes = azx.getMinutes();
                          // let date22 = ("0" + azx.getDate()).slice(-2)
                          // var mon = date.getMonth()+ 1;
                          // var customer_month = a[0].expire_month;
                          // var customer_date = a[0].expire_date;
                          // var customer_hrs = a[0].expire_hrs;
                          // var customer_mins = a[0].expire_mins;
                          // if(customer_month >= mon)
                          // {
                          //   if(customer_date >= date22)
                          //     {
                          //       if(customer_hrs >= hours2)
                          //         {
                          //           if(customer_mins >= minutes)
                          //             {
                          //                 var init_v = 2;
                          //                 var insert_data = db.customers.update(
                          //                   {  
                          //                     free_trial:init_v
                          //                   },  
                          //                   {  where: { api_token: req.body.token } }
                          //                 );

                          //             }
                          //         }
                          //     }
                          // }






                  
                          let customer = await Customer.findAll({
                            where: {
                                id: req.body.user_id,  
                            },
                          });
                          let len =customer.length;
                            if(len>0)
                            {
                              res.json({
                                status: true,
                                data: {
                                  customer,
                                },
                              });
                            }
                            else
                            {
                              res.json({
                                status: false,
                                message:'data not found',
                              });
                            }  
                   } 
            }
};
// //Customer Update Details update_user_details
// exports.update_user_details = async (req, res, next) => {
//   let a = await Customer.findAll({
//     where: { api_token: req.body.token},
//   });
//     if(a.length == 0)
//         {
//             res.json({
//             status: false,
//             message:'Token Not Found',
//                     });
//         }
//         else
//             {
//               let full_name = req.body.full_name;
//               let phone = req.body.phone;
//               //let email = req.body.email;
//               let university_id = req.body.university_id;
//               let degree_id = req.body.degree_id;
//               let semester_id = req.body.semester_id;
//               let user_id = req.body.user_id;
//               let image = req.body.image;

//               let a = await Customer.findAll({
//                 where: { id: user_id},
//               });
//                 if(a.length == 0)
//                     {
//                       return res.json({
//                         status: false,
//                         message: "data not found",
//                       });
//                     }
//                     else
//                     {
//                       var  update = db.customers.update(
//                         { 
//                             full_name: full_name,
//                             phone:phone,
//                            // email:email,
//                             university_id:university_id,
//                             degree_id:degree_id,
//                             semester_id:semester_id,
//                             image:image
//                         },
//                       { where: { id: user_id } }
//                       );
                
//                         return res.json({
//                         status: true,
//                         message: "Customer Details Updated successfully",
//                         data: {user_id:user_id,semester_id:semester_id },
//                                        });
//                   }
      
//                     }

             
// };


//Testing //May 25
//register a new subject
exports.update_user_details = async (req, res, next) => {

  

  try {
    await subjectUpload(req, res);
  } catch (err) {
    console.log(err.message);
    return res.json({ status: false, message: "Could not upload file" });
  }

  try {
    let image = null;
    if (typeof req.files != "undefined") {
      if (
        typeof req.files.uploaded_image != "undefined" &&
        req.files.uploaded_image.length > 0
      ) {
        image = req.files.uploaded_image[0].filename;
      }
    }
 
      
    let a = await Customer.findAll({
      where: { api_token: req.body.token},
    });
      if(a.length == 0)
          {
              res.json({
              status: false,
              message:'Token Not Found',
                      });
          }
          else
              {
                $active_status = a[0].active;
                if($active_status == 0 ||  $active_status == false)
                   {
                   
                     return res.json({
                       status: false,
                       message: "Account Has been Locked.Please Mail at Beyond Theory Admin <admin@beyond-theory.com>",
                                 });
                   }
                   else
                    {
                      let full_name = req.body.full_name;
                      let phone = req.body.phone;
                      //let email = req.body.email;
                      let university_id = req.body.university_id;
                      let degree_id = req.body.degree_id;
                      let semester_id = req.body.semester_id;
                      let user_id = req.body.user_id;
                      let college_id = req.body.college_id;
                      

                      let univer = await College.findAll({
                        where: {
                          university_id: university_id,  
                          id:college_id

                        },
                      });
                      let univer_colege_len =univer.length;
                      if(univer_colege_len!=0 || univer_colege_len!='')
                      {

                     
                            if(image == null)
                              {
                                var z = db.customers.update(
                                  { 
                                    full_name: full_name,
                                    phone:phone,
                                    university_id:university_id,
                                    degree_id:degree_id,
                                    semester_id:semester_id,
                                    college_id:college_id
                                
                                  },
                                  { where: { id: req.body.user_id } }
                                );

                              }
                              else
                                {
                                  var z = db.customers.update(
                                    { 
                                      full_name: full_name,
                                      phone:phone,
                                      university_id:university_id,
                                      degree_id:degree_id,
                                      semester_id:semester_id,
                                      college_id:college_id,
                                      image:image
                                    },
                                    { where: { id: req.body.user_id } }
                                  );
                                }
                      
                            
                            return res.json({
                              status: true,
                              message: "Customer Details Updated successfully",
                              data: {user_id:user_id,semester_id:semester_id },
                                            });

                      }
                      else 
                        {
                          return res.json({
                            status: false,
                            message: "University and College is Not Matched",
                                          });
                        }

                                
                    }                   

              }


      } catch (err) {
        console.log(err);
        res.json({
          status: false,
        });
      }


};
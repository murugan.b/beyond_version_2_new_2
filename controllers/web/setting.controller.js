const db = require("../../models/index");
const Pagination = require("../../utils/pagination");
let express = require("express");
let router = express.Router();
var passwordHash = require('password-hash');


//path /degree
//method GET
//desc degree listing page


//University->College
const College = db.college;
//College->degree
const Degree = db.degree;



exports.page = async (req, res, next) => {
  
    var message = '';
   

  let setting  = await db.admins.findAll({
    
  });

  //var hashedPassword = passwordHash.generate('123456');
  //var hashedPassword = passwordHash.generate('123456');
  //console.log(passwordHash.verify('123456', hashedPassword));
  //console.log(hashedPassword);
  //console.log(setting);

  res.render("setting",{
  
    setting:setting,
    message:message

  });

  
};

exports.password_change = async (req, res, next) => {

    var old_confirm = req.body.old_confirm;
    var old = req.body.old;
    var pass1 = req.body.pass1;
    var pass2 = req.body.pass2;
    let setting  = await db.admins.findAll({ });

    var hashedPassword = old_confirm;
    var check = passwordHash.verify(old, hashedPassword);

    if (check == false)
    {
        message = 'Please Enter Correct  Password ';
        res.render("setting",{
  
            setting:setting,
            message:message
        
          });

    }
    else
    {
            if(pass1 == pass2)
        {
            var hashedPassword = passwordHash.generate(pass1);

                    var z = db.admins.update(
                        { password: hashedPassword },
                        { where: { id: 1 } }
                        );

                        if(z)
                        {       
                                    message = 'Password Changed Success.';
                                    res.render("setting",{
  
                                    setting:setting,
                                    message:message
        
                                                         });

                        }
                

        }
        else
        {
       
             message = "Please Check New and Confirm Password";
             res.render("setting",{
  
             setting:setting,
             message:message
        
                                });

        }

}

};


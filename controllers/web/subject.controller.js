const db = require("../../models/index");
const Pagination = require("../../utils/pagination");
const subjectUpload = require("../../utils/uploads/subject.uploads");
let express = require("express");
let router = express.Router();

//University->College
const College = db.college;
//College->degree
const Degree = db.degree;

const Semester = db.semester;

const Subject = db.subject;

exports.page = async (req, res, next) => {
  //console.log("welcome");
  let page = 0;
  if (req.query.page && req.query.page != 1) {
    page = req.query.page - 1;
  }

  let pageSize = res.locals.pageSize;
  if (req.query.pageSize) {
    page = req.query.pageSize;
  }

  const offset = page * pageSize;
  const limit = pageSize;

  let subject = await db.subject.findAll({
    limit,
    offset,
    where: {},
  });

  let college = await db.college.findAll({
    limit,
    offset,
    where: {
      active: 1,
    },
  });

  let totalCount = await db.semester.count({});

  res.pagination = {
    page: page,
    pageSize,
    totalCount,
  };

  await Pagination(req, res);

  let overall = College.findAll({
    where: { active:1 },
    attributes: ["name"],
    
    include: [
      {
        model: Degree,
        where: { id: db.Sequelize.col("degree.college_id") , active:1},
        attributes: ["name"],
        include: [
          {
            model: Semester,
            where: { degree_id: db.Sequelize.col("degree.id") , active:1},
            attributes: ["name", "id", "active"],
          },
        ],
      },
    ],
  }).then((users) => {
    res.render("subjects", {
      details: users,
      subject: subject,
    });
  });
};

exports.subject_details = async (req, res, next) => {
  //console.log("welcome");
  let page = 0;
  if (req.query.page && req.query.page != 1) {
    page = req.query.page - 1;
  }

  let pageSize = res.locals.pageSize;
  if (req.query.pageSize) {
    page = req.query.pageSize;
  }

  const offset = page * pageSize;
  const limit = pageSize;

  let subject = await db.subject.findAll({
    limit,
    offset,
    where: {},
  });

  let college = await db.college.findAll({
    limit,
    offset,
    where: {
      active: 1,
    },
  });

  let totalCount = await db.semester.count({});

  res.pagination = {
    page: page,
    pageSize,
    totalCount,
  };

  await Pagination(req, res);

  let overall = College.findAll({
    attributes: ["name"],
    include: [
      {
        model: Degree,
        where: { id: db.Sequelize.col("degree.college_id") },
        attributes: ["name"],
        include: [
          {
            model: Semester,
            where: { degree_id: db.Sequelize.col("degree.id") },
            attributes: ["name", "id", "active"],
            include: [
              {
                model: Subject,
                where: { degree_id: db.Sequelize.col("degree.id") },
                attributes: ["name", "id", "active", "price", "image"],
              },
            ],
          },
        ],
      },
    ],
  }).then((users) => {
    res.render("subject_list", {
      details: users,
    });
  });
};

//register a new subject
exports.subject_register = async (req, res, next) => {

  //console.log(req.body);
  try {
    await subjectUpload(req, res);
  } catch (err) {
    console.log(err.message);
    return res.json({ status: false, message: "Could not upload file" });
  }

  try {
    let image = null;
    if (typeof req.files != "undefined") {
      if (
        typeof req.files.uploaded_image != "undefined" &&
        req.files.uploaded_image.length > 0
      ) {
        // image =
        //   req.files.uploaded_image[0].destination +
        //   "/" +
        //   req.files.uploaded_image[0].filename;
        image = req.files.uploaded_image[0].filename;
      }
    }

    let insert = {
      name: req.body.name,
      semester_id: req.body.semester_id,
      price: req.body.price,
      image: image,
    };
    await db.subject.create(insert);
    // return res.json({
    //   status: 1,
    //   message: "Subject added successfully",
    // });
    res.send('<script>window.location.href="/subject_details";</script>');
  } catch (err) {
    console.log(err);
    res.json({
      status: false,
    });
  }
};

//Inactive
exports.subject_inactive = async (req, res, next) => {
  //console.log(req.params.id);
  var z = db.subject.update({ active: 0 }, { where: { id: req.params.id } });

  // console.log(z);
  //res.redirect('/universities');
  res.send('<script>window.location.href="/subject_details";</script>');

  //return res.redirect('/universities');
};

//Active
exports.subject_active = async (req, res, next) => {
  //console.log(req.params.id);
  var z = db.subject.update({ active: 1 }, { where: { id: req.params.id } });

  //console.log(z);
  res.send('<script>window.location.href="/subject_details";</script>');
};

const db = require("../../models/index");
const Pagination = require("../../utils/pagination");
let express = require("express");
let router = express.Router();

//University->College
const College = db.college;
//College->degree
const Degree = db.degree;
const Semester = db.semester;
const Subject = db.subject;
const University = db.university;
const Voucher = db.voucher; 
const Purchased_subject = db.purchased_subject; 
const Customer = db.customers;


//subject->university
const Customer_support = db.customer_support;
//module->college
const Customers = db.customers;
exports.dashboard = async (req, res, next) => {

//University Details  
let university_total_count  = await db.university.findAll({where: { },});
let university_active_count  = await db.university.findAll({where: { active:1 },});
let university_inactive_count  = await db.university.findAll({where: { active:0 },});
var uni_total = university_total_count.length;
var uni_active_total = university_active_count.length;
var uni_inactive_total = university_inactive_count.length;


//College Details  
let college_total_count  = await College.findAll({where: { },});
let college_active_count  = await College.findAll({where: { active:1 },});
let college_inactive_count  = await College.findAll({where: { active:0 },});
var college_total = college_total_count.length;
var college_active_total = college_active_count.length;
var college_inactive_total = college_inactive_count.length;


//User's Details 

let cus_total_count  = await Customer.findAll({where: { },});
let cus_active_count  = await Customer.findAll({where: { active:1 },});
let cus_inactive_count  = await Customer.findAll({where: { active:0 },});
var cus_total = cus_total_count.length;
var cus_active_total = cus_active_count.length;
var cus_inactive_total = cus_inactive_count.length;   


  
 res.render('dashboard',{uni_total,uni_active_total,uni_inactive_total,college_total,college_active_total,
    college_inactive_total,cus_total,cus_active_total,cus_inactive_total});
};
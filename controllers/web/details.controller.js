const db = require('../../models/index');
const Pagination = require('../../utils/pagination');
let express = require('express');
let router = express.Router();

//University->College
const College = db.college;
//College->degree
const Degree = db.degree;

const Semester = db.semester;

const Subject = db.subject;

const University = db.university;

const Voucher = db.voucher;

const Purchased_subject = db.purchased_subject;

//subject->university
const Customer_support = db.customer_support;
//module->college
const Customers = db.customers;

exports.query_details = async (req, res, next) => {
	let page = 0;
	if (req.query.page && req.query.page != 1) {
		page = req.query.page - 1;
	}

	let pageSize = res.locals.pageSize;
	if (req.query.pageSize) {
		page = req.query.pageSize;
	}

	const offset = page * pageSize;
	const limit = pageSize;

	let customer_support = await db.customer_support.findAll({
		where: {
			status: 0,
		},
	});

	let totalCount = await db.customer_support.count({});

	res.pagination = {
		page: page,
		pageSize,
		totalCount,
	};

	await Pagination(req, res);

	let overall = Customers.findAll({
		//where: {status:0 },
		// attributes: [],
		include: [
			{
				model: Customer_support,
				where: {
					id: db.Sequelize.col('customer_support.customer_id'),
					status: 0,
				},
				// attributes: []
			},
		],
	}).then((subjects) => {
		console.log(subjects);

		res.render('query_details', {
			modules: subjects,
		});
	});
};

//Query Completed
exports.query_completed = async (req, res, next) => {
	//console.log(req.params.id);
	var z = db.customer_support.update(
		{ status: 1 },
		{ where: { id: req.params.id } }
	);

	res.send('<script>window.location.href="/query_details";</script>');
};

//Get Customer Details

exports.customer_details = async (req, res, next) => {
	//console.log("welcome");
	let page = 0;
	if (req.query.page && req.query.page != 1) {
		page = req.query.page - 1;
	}

	let pageSize = res.locals.pageSize;
	if (req.query.pageSize) {
		page = req.query.pageSize;
	}

	const offset = page * pageSize;
	const limit = pageSize;

	let customers = await db.customers.findAll({
		limit,
		offset,
		where: {},
	});

	let totalCount = await db.customers.count({});

	res.pagination = {
		page: page,
		pageSize,
		totalCount,
	};

	await Pagination(req, res);

	let overall = University.findAll({
		attributes: ['name'],
		include: [
			{
				model: College,
				where: { id: db.Sequelize.col('college.university_id') },
				attributes: ['name'],
				include: [
					{
						model: Degree,
						where: { college_id: db.Sequelize.col('college.id') },
						attributes: ['name'],

						include: [
							{
								model: Semester,
								where: {
									college_id: db.Sequelize.col('college.id'),
								},
								attributes: ['name'],
								include: [
									{
										model: Customers,
										where: {
											college_id:
												db.Sequelize.col('college.id'),
										},
										attributes: [
											'full_name',
											'id',
											'phone',
											'email',
											'active',
											'email_notify',
										],
									},
								],
							},
						],
					},
				],
			},
		],
	}).then((users) => {
		console.log(users);

		res.render('customer_details', {
			details: users,
		});
	});
};

//Customer Block
exports.customer_block = async (req, res, next) => {
	//console.log(req.params.id);
	var z = db.customers.update(
		{ active: 0 },
		{ where: { id: req.params.id } }
	);

	res.send('<script>window.location.href="/customer_details";</script>');
};

//Customer Unblock
exports.customer_unblock = async (req, res, next) => {
	//console.log(req.params.id);
	var z = db.customers.update(
		{ active: 1 },
		{ where: { id: req.params.id } }
	);

	res.send('<script>window.location.href="/customer_details";</script>');
};

//Search Customer Details search_customer_details

exports.search_customer_details = async (req, res, next) => {
	res.render('search_customer_details');
};

//Get Searched Customer Records search_customer_records

exports.search_customer_records = async (req, res, next) => {
	//console.log("welcome");
	let page = 0;
	if (req.query.page && req.query.page != 1) {
		page = req.query.page - 1;
	}

	let pageSize = res.locals.pageSize;
	if (req.query.pageSize) {
		page = req.query.pageSize;
	}

	const offset = page * pageSize;
	const limit = pageSize;

	let customers = await db.customers.findAll({
		limit,
		offset,
		where: {},
	});

	let totalCount = await db.customers.count({});

	res.pagination = {
		page: page,
		pageSize,
		totalCount,
	};

	await Pagination(req, res);

	let mobile = req.body.mobile;

	let overall = University.findAll({
		attributes: ['name'],
		include: [
			{
				model: College,
				where: { id: db.Sequelize.col('college.university_id') },
				attributes: ['name'],
				include: [
					{
						model: Degree,
						where: { college_id: db.Sequelize.col('college.id') },
						attributes: ['name'],

						include: [
							{
								model: Semester,
								where: {
									college_id: db.Sequelize.col('college.id'),
								},
								attributes: ['name'],
								include: [
									{
										model: Customers,
										where: {
											college_id:
												db.Sequelize.col('college.id'),
											phone: mobile,
										},
										attributes: [
											'full_name',
											'id',
											'phone',
											'email',
											'active',
										],
									},
								],
							},
						],
					},
				],
			},
		],
	}).then((users) => {
		console.log(users);

		res.render('search_customer_reports', {
			details: users,
		});
	});
};

// Voucher Page

exports.voucher = async (req, res, next) => {
	let page = 0;
	if (req.query.page && req.query.page != 1) {
		page = req.query.page - 1;
	}
	let pageSize = res.locals.pageSize;
	if (req.query.pageSize) {
		page = req.query.pageSize;
	}
	const offset = page * pageSize;
	const limit = pageSize;
	let voucher = await db.voucher.findAll({
		limit,
		offset,
		where: {},
	});
	let totalCount = await db.voucher.count({});
	res.pagination = {
		page: page,
		pageSize,
		totalCount,
	};
	await Pagination(req, res);
	//console.log(voucher);
	res.render('voucher', {
		voucher: voucher,
	});
};

// exports.voucher_register = async (req, res, next) => {
// //console.log(req.body);
// db.voucher.create({
//   code: req.body.code,
//   type: req.body.type,
//   value: req.body.value,
// });
// return res.json({
//   status: 1,
//   message: "Coupons added successfully",
// });
// };

//Voucher Inactive
exports.voucher_inactive = async (req, res, next) => {
	var z = db.voucher.update({ active: 0 }, { where: { id: req.params.id } });
	res.send('<script>window.location.href="/voucher";</script>');
};

//Voucher active
exports.voucher_active = async (req, res, next) => {
	var z = db.voucher.update({ active: 1 }, { where: { id: req.params.id } });
	res.send('<script>window.location.href="/voucher";</script>');
};

//Purchase Course Details

exports.purchased_course_details = async (req, res, next) => {
	var id = req.params.id;

	let overall = Subject.findAll({
		//  attributes: ['name'],
		include: [
			{
				model: Purchased_subject,
				where: {
					subject_id: db.Sequelize.col('subject.id'),
					user_id: id,
				},
				//attributes: ['name'],
			},
		],
	}).then((users) => {
		console.log(users);

		res.render('purchase_course_details', {
			details: users,
		});
	});
};

//Payment Process Details  payment_process_details

exports.payment_process_details = async (req, res, next) => {
	var id = req.params.id;
	let page = 0;
	if (req.query.page && req.query.page != 1) {
		page = req.query.page - 1;
	}
	let pageSize = res.locals.pageSize;
	if (req.query.pageSize) {
		page = req.query.pageSize;
	}
	const offset = page * pageSize;
	const limit = pageSize;
	let payment_transaction = await db.payment_transaction.findAll({
		limit,
		offset,
		where: {
			user_id: id,
		},
	});
	let totalCount = await db.payment_transaction.count({});
	res.pagination = {
		page: page,
		pageSize,
		totalCount,
	};
	await Pagination(req, res);
	//console.log(voucher);
	res.render('payment_process_details', {
		details: payment_transaction,
	});
};

exports.voucher_register = async (req, res, next) => {
	//console.log(req.body);
	db.voucher.create({
		code: req.body.code,
		type: req.body.type,
		value: req.body.value,
		remaining_count: req.body.limit,
		mode: req.body.mode,
		purchase_amount: req.body.purchase_amount,
	});
	return res.json({
		status: 1,
		message: 'Coupons added successfully',
	});
};

//May 21
//deep_dive_question_register
exports.deep_dive_question_register = async (req, res, next) => {
	var module_id = req.params.module_id;
	var id = req.params.id;

	let page = 0;
	if (req.query.page && req.query.page != 1) {
		page = req.query.page - 1;
	}
	let pageSize = res.locals.pageSize;
	if (req.query.pageSize) {
		page = req.query.pageSize;
	}
	const offset = page * pageSize;
	const limit = pageSize;
	let deep_dive_question = await db.deep_dive_question.findAll({
		limit,
		offset,
		where: {
			deep_dives_id: id,
		},
	});
	let totalCount = await db.deep_dive_question.count({});
	res.pagination = {
		page: page,
		pageSize,
		totalCount,
	};

	var module_id = req.params.module_id;
	await Pagination(req, res);
	res.render('deep_dive_question_register', {
		quick_prep_resource: deep_dive_question,
		module_id: module_id,
		deep_dives_id: id,
	});
};
//deep_dive_question_register
exports.deep_dive_question_insert = async (req, res, next) => {
	db.deep_dive_question.create({
		question_sub_title: req.body.question_sub_title,
		module_id: req.body.module_id,
		deep_dives_id: req.body.deep_dives_id,
	});
	return res.json({
		status: 1,
		message: 'Questions  are Added added successfully',
	});
};

//May 21
//deep_question_inactive

exports.deep_question_inactive = async (req, res, next) => {
	var z = db.deep_dive_question.update(
		{ active: 0 },
		{ where: { id: req.params.id } }
	);
	res.send('<script>window.location.href="/deep_dives_details";</script>');
};

//deep_question_active

exports.deep_question_active = async (req, res, next) => {
	var z = db.deep_dive_question.update(
		{ active: 1 },
		{ where: { id: req.params.id } }
	);
	res.send('<script>window.location.href="/deep_dives_details";</script>');
};

//Deep Dive Question Edit
exports.deep_dive_question_edit = async (req, res, next) => {
	let deep_dive_question = await db.deep_dive_question.findAll({
		where: { id: req.params.id },
	});
	res.render('deep_dive_question_edit', {
		quick_prep_resource: deep_dive_question,
	});
};

//deep_dive_question_update
exports.deep_dive_question_update = async (req, res, next) => {
	var z = db.deep_dive_question.update(
		{
			question_sub_title: req.body.question_sub_title,
		},
		{ where: { id: req.body.id } }
	);
	res.send('<script>window.location.href="/deep_dives_details";</script>');
};

//Deep Dive Link
//deep_dive_question_register
exports.deep_dive_link_view = async (req, res, next) => {
	var question_id = req.params.id;
	let page = 0;
	if (req.query.page && req.query.page != 1) {
		page = req.query.page - 1;
	}
	let pageSize = res.locals.pageSize;
	if (req.query.pageSize) {
		page = req.query.pageSize;
	}
	const offset = page * pageSize;
	const limit = pageSize;
	let deep_dive_question_details =
		await db.deep_dive_question_details.findAll({
			limit,
			offset,
			where: {
				question_id: question_id,
			},
		});
	let totalCount = await db.deep_dive_question.count({});
	res.pagination = {
		page: page,
		pageSize,
		totalCount,
	};

	var question_id = req.params.id;
	await Pagination(req, res);
	res.render('deep_dive_link', {
		quick_prep_resource: deep_dive_question_details,
		question_id: question_id,
	});
};

//deep_dive_link_insert -Jun 9
exports.deep_dive_link_insert = async (req, res, next) => {
	db.deep_dive_question_details.create({
		question_id: req.body.question_id,
		name: req.body.name,
		link: req.body.link,
		type: req.body.type,
		marque: req.body.marque,
		description1: req.body.description1,
		description2: req.body.description2,
		cms_details: req.body.cms,
	});

	let b = await db.deep_dive_question.findAll({
		where: { id: req.body.question_id },
	});
	$se_id = b[0].video_count + 1;
	$rr = b[0].pdf_count + 1;

	if (req.body.type == 1) {
		var z = db.deep_dive_question.update(
			{
				video_count: $se_id,
			},
			{ where: { id: req.body.question_id } }
		);
	} else {
		var z = db.deep_dive_question.update(
			{
				pdf_count: $rr,
			},
			{ where: { id: req.body.question_id } }
		);
	}

	return res.json({
		status: 1,
		message: "Question Link's  are Added added successfully",
	});
};

//deep_dive_link_edit
exports.deep_dive_link_edit = async (req, res, next) => {
	let deep_dive_question_details =
		await db.deep_dive_question_details.findAll({
			where: { id: req.params.id },
		});
	res.render('deep_dive_link_edit', {
		quick_prep_resource: deep_dive_question_details,
	});
};

//deep_dive_link_update   -may14
exports.deep_dive_link_update = async (req, res, next) => {
	var z = db.deep_dive_question_details.update(
		{
			link: req.body.link,
			name: req.body.name,
			marque: req.body.marque,
			description1: req.body.description1,
			description2: req.body.description2,
			cms_details: req.body.cms_details,
		},
		{ where: { id: req.body.id } }
	);
	res.send('<script>window.location.href="/deep_dives_details";</script>');
};

//deep_dive_link_inactive

exports.deep_dive_link_inactive = async (req, res, next) => {
	var z = db.deep_dive_question_details.update(
		{ active: 0 },
		{ where: { id: req.params.id } }
	);
	res.send('<script>window.location.href="/deep_dives_details";</script>');
};

exports.deep_dive_link_active = async (req, res, next) => {
	var z = db.deep_dive_question_details.update(
		{ active: 1 },
		{ where: { id: req.params.id } }
	);
	res.send('<script>window.location.href="/deep_dives_details";</script>');
};

//May 18

//deep_dive_link_Lock and Unlock
exports.deep_dive_link_lock = async (req, res, next) => {
	var z = db.deep_dive_question_details.update(
		{ locked: 1 },
		{ where: { id: req.params.id } }
	);
	res.send('<script>window.location.href="/deep_dives_details";</script>');
};

exports.deep_dive_link_unlock = async (req, res, next) => {
	var z = db.deep_dive_question_details.update(
		{ locked: 0 },
		{ where: { id: req.params.id } }
	);
	res.send('<script>window.location.href="/deep_dives_details";</script>');
};

//Mail Notify Status Updation

exports.customer_email_notify_active = async (req, res, next) => {
	//console.log(req.params.id);
	var z = db.customers.update(
		{ email_notify: 1 },
		{ where: { id: req.params.id } }
	);
	res.send('<script>window.location.href="/customer_details";</script>');
};
exports.customer_email_notify_inactive = async (req, res, next) => {
	//console.log(req.params.id);
	var z = db.customers.update(
		{ email_notify: 0 },
		{ where: { id: req.params.id } }
	);
	res.send('<script>window.location.href="/customer_details";</script>');
};

// Mail Notify Status Updation  End

//Murugan Start

exports.customer_email_notify_all = async (req, res, next) => {
	//console.log(req.params.id);
	var z = db.customers.update({ email_notify: 1 }, { where: {} });
	res.send('<script>window.location.href="/customer_details";</script>');
};
exports.customer_email_notify_inactive_all = async (req, res, next) => {
	//console.log(req.params.id);
	var z = db.customers.update({ email_notify: 0 }, { where: {} });
	res.send('<script>window.location.href="/customer_details";</script>');
};

exports.hide_free_trail = async (req, res, next) => {
	//console.log(req.params.id);
	var z = db.customers.update({ enable_free_trial: 0 }, { where: {} });
	res.send('<script>window.location.href="/customer_details";</script>');
};

exports.show_free_trail = async (req, res, next) => {
	//console.log(req.params.id);
	var z = db.customers.update({ enable_free_trial: 1 }, { where: {} });
	res.send('<script>window.location.href="/customer_details";</script>');
};

//Murugan End

const db = require("../../models/index");
const Pagination = require("../../utils/pagination");
let express = require("express");
let router = express.Router();




//University->College
const College = db.college;
//College->degree
const Degree = db.degree;

const Semester = db.semester;




exports.page = async (req, res, next) => {
   //console.log("welcome");
   let page = 0;
   if (req.query.page && req.query.page != 1) {
     page = req.query.page - 1;
   }
 
   let pageSize = res.locals.pageSize;
   if (req.query.pageSize) {
     page = req.query.pageSize;
   }
 
   const offset = page * pageSize;
   const limit = pageSize;
 
  //  let semester = await db.semester.findAll({
  //    limit,
  //    offset,
  //    where: {},
  //  });
 
   let college = await db.college.findAll({
    limit,
    offset,
    where: {
        active:1
    },
  });

 
   let totalCount = await db.semester.count({});
 
   res.pagination = {
     page: page,
     pageSize,
     totalCount,
   };
 
   await Pagination(req, res);



 

   let  overall  =  Degree.findAll({
    attributes: ['name','id'],
    include: [{ 
      model: College,
      where: { id: db.Sequelize.col('degree.college_id') },
      attributes: ['name']
    }]
   

  }).then(subjects => {
 
     console.log(subjects);
  
     res.render("semester",{
      degree:subjects
      
      
   
     });
  
     
   });

   
};


  //semester_details
  exports.semester_details = async (req, res, next) => {

      //console.log("welcome");
  let page = 0;
  if (req.query.page && req.query.page != 1) {
    page = req.query.page - 1;
  }

  let pageSize = res.locals.pageSize;
  if (req.query.pageSize) {
    page = req.query.pageSize;
  }

  const offset = page * pageSize;
  const limit = pageSize;


  let college = await db.college.findAll({
   limit,
   offset,
   where: {
       active:1
   },
 });


  let totalCount = await db.semester.count({});

  res.pagination = {
    page: page,
    pageSize,
    totalCount,
  };

  await Pagination(req, res);




 let  overall  =  College.findAll({
   attributes: ['name'],
   include: [
     {
       model: Degree,
       where: { id: db.Sequelize.col('degree.college_id') },
       attributes: ['name'],
       include: [
         {
           model: Semester,
           where: { degree_id: db.Sequelize.col('degree.id') },
           attributes: ['name','id','active'],
         }
       ]
     }

   ]
   

   
 }).then(users => {

   


   res.render("semester_list",{
     semester:users
   
   });
 
 });

  };



exports.semester_register = async (req, res, next) => {
  console.log(req.body);
  db.semester.create({
    name: req.body.name,
    degree_id:req.body.degree_id,
  });
  return res.send({
    status: 1,
    message: "Semester added successfully",
  });
};




//Inactive
exports.semester_inactive = async (req, res, next) => {
  //console.log(req.params.id);
    var z = db.semester.update(
      { active: 0 },
    { where: { id: req.params.id } }
    );
  
   // console.log(z);
    //res.redirect('/universities');
    res.send('<script>window.location.href="/semester_details";</script>');
  
  
    //return res.redirect('/universities');
  };
  
  //Active
  exports.semester_active = async (req, res, next) => {
    //console.log(req.params.id);
      var z = db.semester.update(
        { active: 1 },
      { where: { id: req.params.id } }
      );
    
      //console.log(z);
      res.send('<script>window.location.href="/semester_details";</script>');
    };
     




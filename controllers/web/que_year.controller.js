const db = require("../../models/index");
const Pagination = require("../../utils/pagination");
let express = require("express");
let router = express.Router();




//University->College
const College = db.college;
//College->degree
const Degree = db.degree;

const Semester = db.semester;
const Subject = db.subject;

const Module = db.module;

const Deep_dive = db.deep_dive;

const Quick_prep_index = db.quick_prep_index;


exports.page = async (req, res, next) => {
  
//console.log("welcome");
let page = 0;
if (req.query.page && req.query.page != 1) {
  page = req.query.page - 1;
}

let pageSize = res.locals.pageSize;
if (req.query.pageSize) {
  page = req.query.pageSize;
}

const offset = page * pageSize;
const limit = pageSize;

let subject = await db.subject.findAll({
  limit,
  offset,
  where: {},
});



let totalCount = await db.semester.count({});

res.pagination = {
  page: page,
  pageSize,
  totalCount,
};

await Pagination(req, res);



let  overall  =  College.findAll({
 attributes: ['name'],
 include: [
   {
     model: Degree,
     where: { id: db.Sequelize.col('degree.college_id') },
     attributes: ['name'],
     include: [
       {
         model: Semester,
         where: { degree_id: db.Sequelize.col('degree.id') },
         attributes: ['name','id','active'],
         include: [
          {
            model: Subject,
            where: { degree_id: db.Sequelize.col('degree.id') },
            attributes: ['name','id','active'],
            
           
       }
     ]
    }
  ]
   }

 ]
 

 
}).then(users => {

  //console.log(users);
 
  res.render("exam_year_list",{
    details:users
    
 
  });



 });

  
};


//quick_pre_full_details

exports.quick_pre_full_details = async (req, res, next) => {

  //console.log("welcome");
let page = 0;
if (req.query.page && req.query.page != 1) {
  page = req.query.page - 1;
}

let pageSize = res.locals.pageSize;
if (req.query.pageSize) {
  page = req.query.pageSize;
}

const offset = page * pageSize;
const limit = pageSize;

let subject = await db.subject.findAll({
  limit,
  offset,
  where: {},
});



let totalCount = await db.semester.count({});

res.pagination = {
  page: page,
  pageSize,
  totalCount,
};

await Pagination(req, res);



let  overall  =  College.findAll({
 attributes: ['name'],
 include: [
   {
     model: Degree,
     where: { id: db.Sequelize.col('degree.college_id') },
     attributes: ['name'],
     include: [
       {
         model: Semester,
         where: { degree_id: db.Sequelize.col('degree.id') },
         attributes: ['name','id','active'],
         include: [
          {
            model: Subject,
            where: { degree_id: db.Sequelize.col('degree.id') },
            attributes: ['name','id','active'],
            include: [
              {
                model: Quick_prep_index,
                where: { degree_id: db.Sequelize.col('degree.id') },
                attributes: ['exam_year','id','active','exam_month','download_link','locked'],
              }
            ]
            
           
       }
     ]
    }
  ]
   }

 ]
 

 
}).then(users => {

  console.log(users);
 
  res.render("quick_pre_full_details",{
    details:users
    
 
  });



 });



};

//Register Details 

exports.exam_year_register = async (req, res, next) => {
    //console.log(req.body);
    db.quick_prep_index.create({
      exam_year: req.body.exam_year,
      exam_month:req.body.exam_month,
      subject_id:req.body.subject_id,
      download_link:req.body.download_link,
    });
    return res.json({
      status: 1,
      message: "Year and Month added successfully",
    });
  };
  



  exports.exam_year_inactive = async (req, res, next) => {
    //console.log(req.params.id);
      var z = db.quick_prep_index.update(
        { active: 0 },
      { where: { id: req.params.id } }
      );
    
     // console.log(z);
      //res.redirect('/universities');
      res.send('<script>window.location.href="/quick_pre_full_details";</script>');
    
    
      //return res.redirect('/universities');
    };
    
    //Active
    exports.exam_year_active = async (req, res, next) => {
      //console.log(req.params.id);
        var z = db.quick_prep_index.update(
          { active: 1 },
        { where: { id: req.params.id } }
        );
      
        //console.log(z);
        res.send('<script>window.location.href="/quick_pre_full_details";</script>');
      };
     
      

      //Block Modules
exports.exam_year_block = async (req, res, next) => {
    //console.log(req.params.id);
      var z = db.quick_prep_index.update(
        { locked: 1 },
      { where: { id: req.params.id } }
      );
    
     // console.log(z);
      //res.redirect('/universities');
      res.send('<script>window.location.href="/quick_pre_full_details";</script>');
    
    
      //return res.redirect('/universities');
    };
    
    //Un Block Modules
    exports.exam_year_unblock = async (req, res, next) => {
      //console.log(req.params.id);
        var z = db.quick_prep_index.update(
          { locked: 0 },
        { where: { id: req.params.id } }
        );
      
        //console.log(z);
        res.send('<script>window.location.href="/quick_pre_full_details";</script>');
      };



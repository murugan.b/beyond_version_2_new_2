const db = require("../../models/index");
const Pagination = require("../../utils/pagination");
let express = require("express");
let router = express.Router();

//path /universities
//method GET
//desc universities listing page
exports.page = async (req, res, next) => {
  //console.log("welcome");
  let page = 0;
  if (req.query.page && req.query.page != 1) {
    page = req.query.page - 1;
  }

  let pageSize = res.locals.pageSize;
  if (req.query.pageSize) {
    page = req.query.pageSize;
  }

  const offset = page * pageSize;
  const limit = pageSize;

  let universities = await db.university.findAll({
    limit,
    offset,
    where: {},
   

  });

  let totalCount = await db.university.count({});

  res.pagination = {
    page: page,
    pageSize,
    totalCount,
  };

  await Pagination(req, res);

  // return res.json({ universities, totalCount, query, path:url_parts.path, pagination});

  //console.log(totalCount);

  res.render("universities",{
    universities:universities
  });
};

//path /university
//method POST
//desc new university
exports.new = async (req, res, next) => {
  //console.log(req.body);
  db.university.create({
    name: req.body.name,
  });
  return res.json({
    status: 1,
    message: "University added successfully",
  });
};

// db.university.findAll({
//   limit,
//   offset,
//   where: {},
 

// });



//Inactive
exports.university_inactive = async (req, res, next) => {
console.log(req.params.id);
  var z = db.university.update(
    { active: 0 },
  { where: { id: req.params.id } }
  );

 // console.log(z);
  //res.redirect('/universities');
  res.send('<script>window.location.href="/universities";</script>');


  //return res.redirect('/universities');
};

//Active
exports.university_active = async (req, res, next) => {
  console.log(req.params.id);
    var z = db.university.update(
      { active: 1 },
    { where: { id: req.params.id } }
    );
  
    //console.log(z);
    res.send('<script>window.location.href="/universities";</script>');
  };
  
 
//List out the College Details 

exports.college_list = async (req, res, next) => {

  var id = req.params.id; 

  let page = 0;
  if (req.query.page && req.query.page != 1) {
    page = req.query.page - 1;
  }

  let pageSize = res.locals.pageSize;
  if (req.query.pageSize) {
    page = req.query.pageSize;
  }

  const offset = page * pageSize;
  const limit = pageSize;

  let college = await db.college.findAll({
    limit,
    offset,
    where: {
      university_id:id
    },
   

  });

  let university2 = await db.university.findAll({
    limit,
    offset,
    where: {
      id:id
    },
   

  });




  let totalCount = await db.university.count({});

  res.pagination = {
    page: page,
    pageSize,
    totalCount,
  };

  await Pagination(req, res);

  // return res.json({ universities, totalCount, query, path:url_parts.path, pagination});

  //console.log(totalCount);

  res.render("college_list",{
    universities:college,
    university2:university2

  });


  

};





//Other's Details add

exports.others = async (req, res, next) => {
  //console.log("welcome");
  let page = 0;
  if (req.query.page && req.query.page != 1) {
    page = req.query.page - 1;
  }

  let pageSize = res.locals.pageSize;
  if (req.query.pageSize) {
    page = req.query.pageSize;
  }

  const offset = page * pageSize;
  const limit = pageSize;

  let other = await db.other.findAll({
    limit,
    offset,
    where: {},
   

  });

  let totalCount = await db.other.count({});

  res.pagination = {
    page: page,
    pageSize,
    totalCount,
  };

  await Pagination(req, res);

  // return res.json({ universities, totalCount, query, path:url_parts.path, pagination});

  //console.log(totalCount);

  res.render("others",{
    other:other
  });
};

//other_register
exports.other_register = async (req, res, next) => {
  //console.log(req.body);
  db.other.create({
    name: req.body.name,
  });
  return res.json({
    status: 1,
    message: "Degree added successfully",
  });
};

//Inactive
exports.other_inactive = async (req, res, next) => {
  console.log(req.params.id);
    var z = db.other.update(
      { active: 0 },
    { where: { id: req.params.id } }
    );
  
   // console.log(z);
    //res.redirect('/universities');
    res.send('<script>window.location.href="/others";</script>');
  
  
    //return res.redirect('/universities');
  };
  
  //Active
  exports.other_active = async (req, res, next) => {
    console.log(req.params.id);
      var z = db.other.update(
        { active: 1 },
      { where: { id: req.params.id } }
      );
    
      //console.log(z);
      res.send('<script>window.location.href="/others";</script>');
    };
    
    //other_edit
    exports.other_edit = async (req, res, next) => {
      let other = await db.other.findAll({where: {id:req.params.id},});
        res.render("other_edit",{
        other:other
      });
    };
    //other_update
    exports.other_update = async (req, res, next) => {
      //console.log(req.body.name);
        var z = db.other.update(
          { name: req.body.name },
        { where: { id: req.body.other_id } }
        );
        res.send('<script>window.location.href="/others";</script>');
      };
    
      
const db = require("../../models/index");
const Pagination = require("../../utils/pagination");
let express = require("express");
let router = express.Router();

//subject->university
const University = db.university;
//module->college
const College = db.college;

exports.page = async (req, res, next) => {


 
 let page = 0;
 if (req.query.page && req.query.page != 1) {
   page = req.query.page - 1;
 }

 let pageSize = res.locals.pageSize;
 if (req.query.pageSize) {
   page = req.query.pageSize;
 }

 const offset = page * pageSize;
 const limit = pageSize;

 let modules = await db.modules.findAll({

   limit,
   offset,
   where: {},
  
 });

 let university  = await db.university.findAll({
   where: {
       active:1
   },
 });

 let totalCount = await db.modules.count({});

//   let degree2 = await db.degree.findAll({
//     limit,
//     offset,
//     where: {},
//   });

 res.pagination = {
   page: page,
   pageSize,
   totalCount,
 };

 await Pagination(req, res);

 // return res.json({ degree, totalCount, query, path:url_parts.path, pagination});


 let overall  =  University.findAll({
   attributes: ['name', 'active','id'],
   include: [{
     model: College,
     where: { university_id: db.Sequelize.col('university.id') },
     attributes: ['name','id', 'university_id','active']
   }]
 }).then(subjects => {

  // console.log(subjects);

   res.render("colleges",{
     modules:subjects,
     university:university
 
   });

   
 });


};

//Register Colleges

exports.college_register = async (req, res, next) => {
    //console.log(req.body);
    db.college.create({
      name: req.body.name,
      university_id:req.body.university_id,
    });
    return res.json({
      status: 1,
      message: "College added successfully",
    });
  };
  


  //Inactive
exports.college_inactive = async (req, res, next) => {
    //console.log(req.params.id);
      var z = db.college.update(
        { active: 0 },
      { where: { id: req.params.id } }
      );
    
     // console.log(z);
      //res.redirect('/universities');
      res.send('<script>window.location.href="/colleges";</script>');
    
    
      //return res.redirect('/universities');
    };
    
    //Active
    exports.college_active = async (req, res, next) => {
      //console.log(req.params.id);
        var z = db.college.update(
          { active: 1 },
        { where: { id: req.params.id } }
        );
      
        //console.log(z);
        res.send('<script>window.location.href="/colleges";</script>');
      };



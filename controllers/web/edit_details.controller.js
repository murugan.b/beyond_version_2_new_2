const db = require("../../models/index");
const Pagination = require("../../utils/pagination");
const subjectUpload = require("../../utils/uploads/subject.uploads");
let express = require("express");
let router = express.Router();


const College = db.college;
const Degree = db.degree;
const Semester = db.semester;
const Subject = db.subject;
const University = db.university;
const Voucher = db.voucher; 
const Purchased_subject = db.purchased_subject; 
const Customer_support = db.customer_support;
const Customers = db.customers;

//university_edit
exports.university_edit = async (req, res, next) => {
    let universities = await db.university.findAll({where: {id:req.params.id},});
      res.render("university_edit",{
      universities:universities
    });
  };
  //university_update
  exports.university_update = async (req, res, next) => {
    //console.log(req.body.name);
      var z = db.university.update(
        { name: req.body.name },
      { where: { id: req.body.university_id } }
      );
      res.send('<script>window.location.href="/universities";</script>');
    };
  
//college_edit


exports.college_edit = async (req, res, next) => {

let university  = await db.university.findAll({
  where: {
      active:1
  },
});

let overall  =  University.findAll({
  attributes: ['name', 'active','id'],
  include: [{
    model: College,
    where: { university_id: db.Sequelize.col('university.id') ,id: req.params.id},
    attributes: ['name','id', 'university_id','active']
  }]
}).then(subjects => {
//console.log(subjects);
  res.render("college_edit",{
    modules:subjects,
    university:university

  });
});

};
//college_update
exports.college_update = async (req, res, next) => {
  var old_university_id = req.body.old_university_id;
  var university_id = req.body.university_id;

  if (old_university_id == university_id)
  {
    var z = db.college.update(
      { name: req.body.name },
    { where: { id: req.body.college_id } }
    );
    res.send('<script>window.location.href="/colleges";</script>');

  }
  else
  {
    var z = db.college.update(
      { name: req.body.name,
      university_id:university_id },
    { where: { id: req.body.college_id } }
    );
    res.send('<script>window.location.href="/colleges";</script>');
  }
    
  };

  //degree_edit
  exports.degree_edit = async (req, res, next) => {
    let degree = await db.degree.findAll({where: {id:req.params.id},});
      res.render("degree_edit",{
        degree:degree
    });
  };
//degree_update
exports.degree_update = async (req, res, next) => {
  //console.log(req.body.name);
    var z = db.degree.update(
      { name: req.body.name },
    { where: { id: req.body.degree_id } }
    );
    res.send('<script>window.location.href="/degrees";</script>');
  };


 //semester_edit
 exports.semester_edit = async (req, res, next) => {
  let semester = await db.semester.findAll({where: {id:req.params.id},});
    res.render("semester_edit",{
      semester:semester
  });
};
//semester_update
exports.semester_update = async (req, res, next) => {
  //console.log(req.body.name);
    var z = db.semester.update(
      { name: req.body.name },
    { where: { id: req.body.semester_id } }
    );
    res.send('<script>window.location.href="/semester_details";</script>');
  };

 //subject_edit 
 exports.subject_edit = async (req, res, next) => {
  let subject = await db.subject.findAll({where: {id:req.params.id},});
    res.render("subject_edit",{
      subject:subject
  });
};

//subject_update
exports.subject_update = async (req, res, next) => {
//console.log(req.body.name);
 var z = db.subject.update(
      { name: req.body.name,
        price:req.body.price 
      },
      { where: { id: req.body.subject_id } });
    res.send('<script>window.location.href="/subject_details";</script>');
};

//subject_image_edit
exports.subject_image_edit = async (req, res, next) => {
  let subject = await db.subject.findAll({where: {id:req.params.id},});
    res.render("subject_edit_image",{
      subject:subject
  });
};

//Subject Image Update
exports.subject_image_update = async (req, res, next) => {


  try {
    await subjectUpload(req, res);
  } catch (err) {
    console.log(err.message);
    return res.json({ status: false, message: "Could not upload file" });
  }

  try {
    let image = null;
    if (typeof req.files != "undefined") {
      if (
        typeof req.files.uploaded_image != "undefined" &&
        req.files.uploaded_image.length > 0
      ) {
        // image =
        //   req.files.uploaded_image[0].destination +
        //   "/" +
        //   req.files.uploaded_image[0].filename;
        image = req.files.uploaded_image[0].filename;
      }
    }

    var z = db.subject.update(
      { 
        image:image
      },
    { where: { id: req.body.subject_id } }
    );
    res.send('<script>window.location.href="/subject_details";</script>');

  } catch (err) {
    console.log(err);
    res.json({
      status: false,
    });
  }







    

};

//module_edit
exports.module_edit = async (req, res, next) => {
  let module = await db.module.findAll({where: {id:req.params.id},});
    res.render("module_edit",{
      module:module
  });
};
//module_update
exports.module_update = async (req, res, next) => {

  var z = db.module.update(
       { 
         name: req.body.name
         
       },
       { where: { id: req.body.module_id } });
     res.send('<script>window.location.href="/module_details";</script>');
 };
 //module_image_edit
 exports.module_image_edit = async (req, res, next) => {
  let module = await db.module.findAll({where: {id:req.params.id},});
    res.render("module_image_edit",{
      module:module
  });
};

//module_image_update
exports.module_image_update = async (req, res, next) => {


  

  try {
    await subjectUpload(req, res);
  } catch (err) {
    console.log(err.message);
    return res.json({ status: false, message: "Could not upload file" });
  }

  try {
    let image = null;
    if (typeof req.files != "undefined") {
      if (
        typeof req.files.uploaded_image != "undefined" &&
        req.files.uploaded_image.length > 0
      ) {
        // image =
        //   req.files.uploaded_image[0].destination +
        //   "/" +
        //   req.files.uploaded_image[0].filename;
        image = req.files.uploaded_image[0].filename;
      }
    }

    var z = db.module.update(
      { 
        image:image
      },
      { where: { id: req.body.module_id } }
    );
    res.send('<script>window.location.href="/module_details";</script>');

  } catch (err) {
    console.log(err);
    res.json({
      status: false,
    });
  }




  

        

  
  };

 
//deep_dive_content_edit
exports.deep_dive_content_edit = async (req, res, next) => {
  let deep_dive = await db.deep_dive.findAll({where: {id:req.params.id},});
    res.render("deep_dive_content_edit",{
      deep_dive:deep_dive
  });
};

//deep_dive_content_update
exports.deep_dive_content_update = async (req, res, next) => {

  var z = db.deep_dive.update(
       { 
         content_title: req.body.name
         
       },
       { where: { id: req.body.deep_dive_id } });
     res.send('<script>window.location.href="/deep_dives_details";</script>');
 };

 //quick_prep_full_details_edit
 exports.quick_prep_full_details_edit = async (req, res, next) => {
  let quick_prep_index = await db.quick_prep_index.findAll({where: {id:req.params.id},
    attributes: ['exam_year','id', 'exam_month','download_link']
  });
  res.render("quick_prep_full_details_edit",{
    quick_prep_index:quick_prep_index
        });

 };
//quick_prep_full_details_update
exports.quick_prep_full_details_update = async (req, res, next) => {

  var z = db.quick_prep_index.update(
       { 
        exam_year: req.body.exam_year,
        exam_month:req.body.exam_month,
        download_link:req.body.download_link
         
       },
       { where: { id: req.body.quick_prep_index_id } });
     res.send('<script>window.location.href="/quick_pre_full_details";</script>');
 };

//question_edit 
exports.question_edit = async (req, res, next) => {
  let quick_prep = await db.quick_prep.findAll({where: {id:req.params.id},
  });
  res.render("question_edit",{
    quick_prep:quick_prep
        });

 };

//question_update 
exports.question_update = async (req, res, next) => {

  var z = db.quick_prep.update(
       { 
        question_title: req.body.question_title,
        question_description:req.body.question_description
       
         
       },
       { where: { id: req.body.quick_prep_id } });
     res.send('<script>window.location.href="/quick_pre_full_details";</script>');
 };

 //question_sub_edit
 exports.question_sub_edit = async (req, res, next) => {
  let quick_prep_resource = await db.quick_prep_resource.findAll({where: {id:req.params.id},
  });
  res.render("question_sub_edit",{
    quick_prep_resource:quick_prep_resource
        });

 };

 //question_sub_update
 exports.question_sub_update = async (req, res, next) => {

  var z = db.quick_prep_resource.update(
       { 
        question_sub_title: req.body.question_sub_title,
        link:req.body.link,
        marque:req.body.marque, description1:req.body.description1, description2:req.body.description2,cms_details:req.body.cms
       
         
       },
       { where: { id: req.body.quick_prep_resource_id } });
     res.send('<script>window.location.href="/quick_pre_full_details";</script>');
 };

 //  //version_edit
exports.version_edit = async (req, res, next) => {
  let universities = await db.version.findAll();
    res.render("version_edit",{
    universities:universities
  });
};
// //version_update
exports.version_update= async (req, res, next) => {
  //console.log(req.body.name);
    var z = db.version.update(
      { name: req.body.name },
    { where: { id: req.body.version_id } }
    );
    res.send('<script>window.location.href="/version_edit";</script>');
  };

  
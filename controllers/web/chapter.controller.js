const db = require("../../models/index");
const Pagination = require("../../utils/pagination");
let express = require("express");
let router = express.Router();

//University->College
const College = db.college;
//College->degree
const Degree = db.degree;

const Semester = db.semester;
const Subject = db.subject;

const Module = db.module;

const Deep_dive = db.deep_dive;
const Deep_dive_question = db.deep_dive_question;


exports.page = async (req, res, next) => {
  

 //console.log("welcome");
 let page = 0;
 if (req.query.page && req.query.page != 1) {
   page = req.query.page - 1;
 }
 
 let pageSize = res.locals.pageSize;
 if (req.query.pageSize) {
   page = req.query.pageSize;
 }
 
 const offset = page * pageSize;
 const limit = pageSize;
 
 let subject = await db.subject.findAll({
   limit,
   offset,
   where: {},
 });
 
 
 
 let totalCount = await db.semester.count({});
 
 res.pagination = {
   page: page,
   pageSize,
   totalCount,
 };
 
 await Pagination(req, res);
 
 
 
 let  overall  =  College.findAll({
  attributes: ['name'],
  include: [
    {
      model: Degree,
      where: { id: db.Sequelize.col('degree.college_id') },
      attributes: ['name'],
      include: [
        {
          model: Semester,
          where: { degree_id: db.Sequelize.col('degree.id') },
          attributes: ['name','id','active'],
          include: [
           {
             model: Subject,
             where: { degree_id: db.Sequelize.col('degree.id') },
             attributes: ['name','id','active'],
             include: [
               {
                 model: Module,
                 where: { degree_id: db.Sequelize.col('degree.id') },
                 attributes: ['name','id','active'],
               }
             ]
            
        }
      ]
     }
   ]
    }
 
  ]
  
 
  
 }).then(users => {
 
   //console.log(users);
  
   res.render("deep_dive",{
     details:users
     
  
   });
 
 
 
  });

  
};



//deep_dives_details


exports.deep_dives_details = async (req, res, next) => {

    
 //console.log("welcome");
 let page = 0;
 if (req.query.page && req.query.page != 1) {
   page = req.query.page - 1;
 }
 
 let pageSize = res.locals.pageSize;
 if (req.query.pageSize) {
   page = req.query.pageSize;
 }
 
 const offset = page * pageSize;
 const limit = pageSize;
 
 let subject = await db.subject.findAll({
   limit,
   offset,
   where: {},
 });
 
 
 
 let totalCount = await db.semester.count({});
 
 res.pagination = {
   page: page,
   pageSize,
   totalCount,
 };
 
 await Pagination(req, res);
 
 
 
 let  overall  =  College.findAll({
  attributes: ['name'],
  include: [
    {
      model: Degree,
      where: { id: db.Sequelize.col('degree.college_id') },
      attributes: ['name'],
      include: [
        {
          model: Semester,
          where: { degree_id: db.Sequelize.col('degree.id') },
          attributes: ['name','id','active'],
          include: [
           {
             model: Subject,
             where: { degree_id: db.Sequelize.col('degree.id') },
             attributes: ['name','id','active'],
             include: [
               {
                 model: Module,
                 where: { degree_id: db.Sequelize.col('degree.id') },
                 attributes: ['name','id','active'],
                 include: [
                  {
                    model: Deep_dive,
                    where: { degree_id: db.Sequelize.col('degree.id') },
                    attributes: ['content_title','id','active','locked'],
                  }
                ]
               }
             ]
            
        }
      ]
     }
   ]
    }
 
  ]
  
 
  
 }).then(users => {
 
   console.log(users);
  
   res.render("deep_dives_list",{
     details:users
     
  
   });
 
 
 
  });


};
//Register Details 

exports.content_register = async (req, res, next) => {
   // console.log(req.body);
    db.deep_dive.create({
      module_id: req.body.module_id,
      content_title:req.body.name,
      
    });
    return res.json({
      status: 1,
      message: "Content and Modules are Added added successfully",
    });
  };
  



  exports.content_inactive = async (req, res, next) => {
    //console.log(req.params.id);
      var z = db.deep_dive.update(
        { active: 0 },
      { where: { id: req.params.id } }
      );
    
     // console.log(z);
      //res.redirect('/universities');
      res.send('<script>window.location.href="/deep_dives_details";</script>');
    
    
      //return res.redirect('/universities');
    };
    
    //Active
    exports.content_active = async (req, res, next) => {
      //console.log(req.params.id);
        var z = db.deep_dive.update(
          { active: 1 },
        { where: { id: req.params.id } }
        );
      
        //console.log(z);
        res.send('<script>window.location.href="/deep_dives_details";</script>');
      };
     
      

      //Block Modules
exports.content_block = async (req, res, next) => {
    //console.log(req.params.id);
      var z = db.deep_dive.update(
        { locked: 1 },
      { where: { id: req.params.id } }
      );
    
     // console.log(z);
      //res.redirect('/universities');
      res.send('<script>window.location.href="/deep_dives_details";</script>');
    
    
      //return res.redirect('/universities');
    };
    
    //Un Block Modules
    exports.content_unblock = async (req, res, next) => {
      //console.log(req.params.id);
        var z = db.deep_dive.update(
          { locked: 0 },
        { where: { id: req.params.id } }
        );
      
        //console.log(z);
        res.send('<script>window.location.href="/deep_dives_details";</script>');
      };






//May 23
//Question Register and Manage Details 

 exports.question_page = async (req, res, next) => {

var id = req.params.id;
var subject = req.params.subject;
// console.log(id);
// console.log(subject);
  let page = 0;
 if (req.query.page && req.query.page != 1) {
   page = req.query.page - 1;
 }

 let pageSize = res.locals.pageSize;
 if (req.query.pageSize) {
   page = req.query.pageSize;
 }

 const offset = page * pageSize;
 const limit = pageSize;
 
 let quick_prep_index = await db.quick_prep_index.findAll({
  
   where: {
    
    id:id,
    
   },
   attributes: ['id','exam_year','exam_month','listing_order','subject_id','download_link','locked','active','createdAt','updatedAt'],
 });

 let modules_name = await db.modules.findAll({
 
  where: {
   
   subject_id:subject,
   
  },
});


 let quick_prep = await db.quick_prep.findAll({
  limit,
  offset,
  where: {
    quick_prep_index_id:id,

          },
});



let totalCount = await db.quick_prep.count({});

 
 res.pagination = {
    page: page,
    pageSize,
    totalCount,
  };
 await Pagination(req, res);

 let  overall  =  Module.findAll({
  where: { subject_id: subject },
  include: [
    {
      model: Deep_dive_question,
      where: { module_id: db.Sequelize.col('module.id') },
    }
  ]
 }).then(users => {

  res.render("quick_prep",{

    quick_prep:quick_prep,
    quick_prep_index:quick_prep_index,
    modules_name:users

  });

  });

 };

//Question Register Page 


exports.question_register = async (req, res, next) => {

 
  var  module_topic_id = req.body.module_topic_id;
   let module = await db.deep_dive_question.findAll({where: {id:module_topic_id},});


  var module_id = module[0].module_id;
 let deep_dive = await db.deep_dive.findAll({
    where: {
      module_id:module_id
    
    },
  });



   db.quick_prep.create({
    quick_prep_index_id: req.body.quick_prep_index_id,
    question_title:req.body.question_title,
    question_description:req.body.question_description,
    module_topic_id:module_topic_id
     
   });


   let quick_prep = await db.quick_prep.findAll({
 
    where: {},
  //   order: [
  //     ['id', 'DESC'],
   
  // ],
   
  });

 var  qp_id=quick_prep.length+1;
 

   
    var deep_dive_id=deep_dive[0].id;

   db.quick_prep_deep_dive_link.create({
     quick_prep_id: qp_id,
     module_id:module_id,
     deep_dive_id:deep_dive_id,
     module_topic_id:module_topic_id
     
    });
    return res.json({
      status: 1,
      message: "Questions  are Added added successfully",
    });
 };

 //May 23
 //Question Inactive
 
 exports.question_inactive = async (req, res, next) => {
  //console.log(req.params.id);
    var z = db.quick_prep.update(
      { active: 0 },
    { where: { id: req.params.id } }
    );
  
   // console.log(z);
    //res.redirect('/universities');
    res.send('<script>window.location.href="/quick_pre_full_details";</script>');
  
  
    //return res.redirect('/universities');
  };
  
  //Active Question
  exports.question_active = async (req, res, next) => {
    //console.log(req.params.id);
      var z = db.quick_prep.update(
        { active: 1 },
      { where: { id: req.params.id } }
      );
    
      //console.log(z);
      res.send('<script>window.location.href="/quick_pre_full_details";</script>');
    };
   
    

    //Block Question
exports.question_block = async (req, res, next) => {
  //console.log(req.params.id);
    var z = db.quick_prep.update(
      { locked: 1 },
    { where: { id: req.params.id } }
    );
  
   // console.log(z);
    //res.redirect('/universities');
    res.send('<script>window.location.href="/quick_pre_full_details";</script>');
  
  
    //return res.redirect('/universities');
  };
  
  //Un Block Question
  exports.question_unblock = async (req, res, next) => {
    //console.log(req.params.id);
      var z = db.quick_prep.update(
        { locked: 0 },
      { where: { id: req.params.id } }
      );
    
      //console.log(z);
      res.send('<script>window.location.href="/quick_pre_full_details";</script>');
    };



    //Sub Details Register on Question 

    
    exports.question_fulldetails = async (req, res, next) => {

      var id = req.params.id;
      console.log(id);
        let page = 0;
       if (req.query.page && req.query.page != 1) {
         page = req.query.page - 1;
       }
      
       let pageSize = res.locals.pageSize;
       if (req.query.pageSize) {
         page = req.query.pageSize;
       }
      
       const offset = page * pageSize;
       const limit = pageSize;
      
       let quick_prep = await db.quick_prep.findAll({
         limit,
         offset,
         where: {
          
          id:id,
          
         },
       });
       let quick_prep_resource = await db.quick_prep_resource.findAll({
        limit,
        offset,
        where: {
         
             quick_prep_id:id
      
                },
      });
      
       let totalCount = await db.quick_prep_resource.count({});

       res.pagination = {
          page: page,
          pageSize,
          totalCount,
        };
       await Pagination(req, res);
  
        res.render("quick_prep_resources",{
      
          quick_prep:quick_prep,
          quick_prep_resource:quick_prep_resource
      
        });
      
       };

       //Register Full Details 


       
       exports.question_full_register = async (req, res, next) => {
        //console.log(req.body);
        db.quick_prep_resource.create({
         question_sub_title: req.body.question_sub_title,
         quick_prep_id:req.body.quick_prep_id,
         link:req.body.link,
         type:req.body.type,
         marque:req.body.marque,
         description1:req.body.description1,
         description2:req.body.description2,
         cms_details:req.body.cms,
          
        });

        let b = await db.quick_prep.findAll({
         where: { id: req.body.quick_prep_id},
       });
       $se_id=b[0].video_count+1;
       $rr =b[0].pdf_count+1;
      // console.log($se_id);
       //console.log($rr);
     
       if(req.body.type == 1)
       {
         var z = db.quick_prep.update(
           { 
            video_count:$se_id
           },
           { where: { id: req.body.quick_prep_id } });
       }
       else
         {   
           var z = db.quick_prep.update(
             { 
              pdf_count:$rr
             },
             { where: { id: req.body.quick_prep_id } });
           
         }
 
         


        return res.json({
          status: 1,
          message: "Full Questions  are Added added successfully",
        });
      };



       
 //Question Fulll Inactive
 
 exports.question_full_inactive = async (req, res, next) => {
  //console.log(req.params.id);
    var z = db.quick_prep_resource.update(
      { active: 0 },
    { where: { id: req.params.id } }
    );
  
   // console.log(z);
    //res.redirect('/universities');
    res.send('<script>window.location.href="/quick_pre_full_details";</script>');
  
  
    //return res.redirect('/universities');
  };
  
  //Active Fulll Question 
  exports.question_full_active = async (req, res, next) => {
    //console.log(req.params.id);
      var z = db.quick_prep_resource.update(
        { active: 1 },
      { where: { id: req.params.id } }
      );
    
      //console.log(z);
      res.send('<script>window.location.href="/quick_pre_full_details";</script>');
    };
   
    

    //Block  Fulll Question
exports.question_full_block = async (req, res, next) => {
  //console.log(req.params.id);
    var z = db.quick_prep_resource.update(
      { locked: 1 },
    { where: { id: req.params.id } }
    );
  
   // console.log(z);
    //res.redirect('/universities');
    res.send('<script>window.location.href="/quick_pre_full_details";</script>');
  
  
    //return res.redirect('/universities');
  };
  
  //Un Block Fulll  Question
  exports.question_full_unblock = async (req, res, next) => {
    //console.log(req.params.id);
      var z = db.quick_prep_resource.update(
        { locked: 0 },
      { where: { id: req.params.id } }
      );
    
      //console.log(z);
      res.send('<script>window.location.href="/quick_pre_full_details";</script>');
    };




const db = require("../../models/index");
const Pagination = require("../../utils/pagination");
let express = require("express");
let router = express.Router();

// //File Upload
// var multer = require('multer');
// var upload = multer({dest:'uploads/'});

//path /degree
//method GET
//desc degree listing page


//University->College
const College = db.college;
//College->degree
const Degree = db.degree;



exports.page = async (req, res, next) => {
  //console.log("welcome");
  let page = 0;
  if (req.query.page && req.query.page != 1) {
    page = req.query.page - 1;
  }

  let pageSize = res.locals.pageSize;
  if (req.query.pageSize) {
    page = req.query.pageSize;
  }

  const offset = page * pageSize;
  const limit = pageSize;

  let degree2 = await db.degree.findAll({
    limit,
    offset,
    where: {},
  });

  let college  = await db.college.findAll({
    where: {
        active:1
    },
  });

  let totalCount = await db.degree.count({});

  res.pagination = {
    page: page,
    pageSize,
    totalCount,
  };

  await Pagination(req, res);

  let other = await db.other.findAll({
    limit,
    offset,
    where: {
      active:1,

    },
  });

 
  let overall  =  College.findAll({
    attributes: ['name', 'active','id'],
    include: [{
      model: Degree,
      where: { college_id: db.Sequelize.col('college.id') },
      attributes: ['name','id', 'university_id','active']
    }]
  }).then(subjects => {
 
   // console.log(subjects);
 
    res.render("degree",{
      modules:subjects,
      college:college,
      other:other
  
    });
 
    
  });

  

  
};

//path /degree
//method POST
//desc new degree





exports.degree_register =async (req, res, next) => {

  const degree = req.body.name;

  var test  = req.body.name;
  
  var len =test[1];
  var lenn = len.length;
 
 if ( lenn==1 )
 {
  db.degree.create({
    name: req.body.name,
    college_id:req.body.college_id
  });
 }
 else
 {
   

 degree.forEach(function(degree)
  {
    
  db.degree.create({
    name: degree,
    college_id:req.body.college_id
  });

}); 

}

//  res.send('<script>window.location.href="/degrees";</script>');
return res.json({
  status: 1,
  message: "Degree added successfully",
});
 
 
  
 };



exports.degree_list = async (req, res, next) => {
  //console.log("Checked");
  var id = req.params.id; 
  //console.log(id);
  let page = 0;
  if (req.query.page && req.query.page != 1) {
    page = req.query.page - 1;
  }

  let pageSize = res.locals.pageSize;
  if (req.query.pageSize) {
    page = req.query.pageSize;
  }

  const offset = page * pageSize;
  const limit = pageSize;

  let degree2 = await db.degree.findAll({
    limit,
    offset,
    where: {
      college_id:id
    },
  });

  let college  = await db.college.findAll({
    where: {
        id:id
    },
  });

//console.log(university);
 

  let totalCount = await db.degree.count({});

  res.pagination = {
    page: page,
    pageSize,
    totalCount,
  };

  await Pagination(req, res);

  res.render("degree_list",{
    degree:degree2,
    college:college,
   

  });
};


//Inactive
exports.degree_inactive = async (req, res, next) => {
  //console.log(req.params.id);
    var z = db.degree.update(
      { active: 0 },
    { where: { id: req.params.id } }
    );
  
   // console.log(z);
    //res.redirect('/universities');
    res.send('<script>window.location.href="/degrees";</script>');
  
  
    //return res.redirect('/universities');
  };
  
  //Active
  exports.degree_active = async (req, res, next) => {
    //console.log(req.params.id);
      var z = db.degree.update(
        { active: 1 },
      { where: { id: req.params.id } }
      );
    
      //console.log(z);
      res.send('<script>window.location.href="/degrees";</script>');
    };
     


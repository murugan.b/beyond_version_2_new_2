const db = require("../../models/index");
const Pagination = require("../../utils/pagination");
let express = require("express");
let router = express.Router();

//const db = require('../../models/index.js');
//const Subject = db.customers;
const Subject = db.subject;
//const Module = db.address;
const Module = db.module;

exports.page = async (req, res, next) => {

  
  // Subject.findAll({
  //   attributes: [['id', 'subject_id'], ['name', 'subject_name'], 'active'],
  //   include: [{
  //     model: Module,
  //     where: { subject_id: db.Sequelize.col('subject.id') },
  //     attributes: ['name', 'locked']
  //   }]
  // }).then(subjects => {
  //    res.send(subjects);
  // });
  
  let page = 0;
  if (req.query.page && req.query.page != 1) {
    page = req.query.page - 1;
  }

  let pageSize = res.locals.pageSize;
  if (req.query.pageSize) {
    page = req.query.pageSize;
  }

  const offset = page * pageSize;
  const limit = pageSize;

  let modules = await db.modules.findAll({

    limit,
    offset,
    where: {},
   
  });

  let subject  = await db.subject.findAll({
    where: {
        active:1
    },
  });

  let totalCount = await db.modules.count({});

//   let degree2 = await db.degree.findAll({
//     limit,
//     offset,
//     where: {},
//   });

  res.pagination = {
    page: page,
    pageSize,
    totalCount,
  };

  await Pagination(req, res);

  // return res.json({ degree, totalCount, query, path:url_parts.path, pagination});

 
  let overall  =  Subject.findAll({
    attributes: ['name', 'active'],
    include: [{
      model: Module,
      where: { subject_id: db.Sequelize.col('subject.id') },
      attributes: ['name','id', 'locked','active']
    }]
  }).then(subjects => {

    console.log(subjects);

    res.render("modules",{
      modules:subjects,
      subject:subject
  
    });

    
  });



  
  




   
  

};

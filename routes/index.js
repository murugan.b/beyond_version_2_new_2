var express = require('express');
var router = express.Router();

var webRouter  = require('./web.router');
var apiRouter  = require('./api.router');

/* GET home page. */
router.use('/', webRouter);
router.use('/api', apiRouter);

module.exports = router;

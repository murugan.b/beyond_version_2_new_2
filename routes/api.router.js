let express = require("express");
let router = express.Router();

//middlewares
let isLoggedIn = require('../middlewares/isLoggedIn');

let loginController = require("../controllers/api/login.controller");

let userController = require("../controllers/api/user.controller");

//Default Index Controller
let indexController = require("../controllers/api/index.controller");
//Dashboard Controller
let dashboardController = require("../controllers/api/dashboard.controller");
//All Details Controller
let detailsController = require("../controllers/api/details.controller");

//Verfication Controller 
let verificationController = require("../controllers/api/verification.controller");


//May 27
//Payumoney Controller
let payController =  require("../controllers/api/pay.controller");

//May 27

//Date:02/09/2021,Author: Divya

let referralCodeController = require('../controllers/api/referralCode.controller')

//


//login
router.post("/login", loginController.login);

//Register User Details
router.post("/user", userController.page);

//Get Details on User
router.post("/user_details", userController.user_details);

//Edit Show User Details 
router.post("/edit_user_details", userController.edit_user_details);

//Update User Details 
router.post("/update_user_details", userController.update_user_details);

// Default index Page
router.get("/",indexController.page);


//free trial 

router.post("/free_trial_activated",dashboardController.free_trial_activated);

router.post("/free_trial_activated_check",dashboardController.free_trial_activated_check);

router.post("/free_trial_check",dashboardController.free_trial_check);



//end free trail


//Dashboard Page

router.post("/dashboard",dashboardController.page);

//Get Deep Dive List Details
router.post("/deep_dive_list",detailsController.deep_dive_list);

//Get Deep Dive Question Details Depend Upon Modules
router.post("/deep_dive_question_list",detailsController.deep_dive_question_list);

//Get Deep Dive Full Detailed Question Details Depend Upon Question on Quick Prep
router.post("/deep_dive_question_list_fulldetails",detailsController.deep_dive_question_list_fulldetails);

//Get Quick Prep Year and Month  Depend Upon Subject id
router.post("/get_quick_prep",detailsController.get_quick_prep);

// Get Quck Prep Subject List  
router.post("/get_quick_prep_question_list",detailsController.get_quick_prep_question_list);

//Get Quick Prep  Full Detailed Question Details Depend Upon Question on Quick Prep
router.post("/get_quick_prep_question_list_fulldetails",detailsController.get_quick_prep_question_list_fulldetails);


//Quick Prep Question to Deep Dive Module Page 
router.post("/quickprep_deepdive",detailsController.quickprep_deepdive);

//Customer Support Details 
router.post("/customer_support",detailsController.customer_support);


//Purchased Course Register 
router.post("/purchase_course_register",detailsController.purchase_course_register);

//Payment Transaction Register  
router.post("/payment_transaction",detailsController.payment_transaction);


//Payment Failed     
router.post("/payment_failed",detailsController.payment_failed);



//Listout the Subject Payment Details   
router.post("/subject_payment",detailsController.subject_payment);

//Get register_count_Details
router.post("/register_count_Details",detailsController.register_count_Details);

//Dashboard Functionalities:
router.post("/college_details",indexController.college_details);

router.post("/degree_details",indexController.degree_details);

router.post("/semester_details",indexController.semester_details);

//Deep dive to Quick Prep

router.post("/deepdive_to_quickprep",detailsController.deepdive_to_quickprep);

//Mobile
router.post("/message",verificationController.message);
router.post("/message_verify",verificationController.message_verify);
//Resend
router.post("/resent_message",verificationController.resent_message);

//Email
//Email   - May 11
router.post("/email_sent",verificationController.email_sent);
router.post("/email_verify",verificationController.email_verify);
//Resend
router.post("/email_resent",verificationController.email_resent);

//May 12

router.post("/deep_dive_question_details",detailsController.deep_dive_question_details);

//May 13
//deep_dive_module_list
router.post("/deep_dive_module_list",detailsController.deep_dive_module_list);

//May 21
//Purchase History
router.post("/purchase_history",detailsController.purchase_history);

//Contact Us

router.post("/contact_us",verificationController.contact_us);

//May 21

//May 22

router.post("/deep_to_quick_first",detailsController.deep_to_quick_first);

//May 27
//PayuMoney 
router.post('/payment/payumoney',payController.payUMoneyPayment);
router.post('/payment/payumoney/response',payController.payUMoneyPaymentResponse);

//May 27

//Jun 9 
router.post('/mobile_number_verification_message',verificationController.mobile_number_verification_message);
router.post('/mobile_number_verification_message_verify',verificationController.mobile_number_verification_message_verify);
//Jun 9

//Jun 11
router.post('/coupons_user_verify',detailsController.coupons_user_verify);
//Jun 11

//July 9

router.post('/version_checking',detailsController.version_checking);

//July 9

//July 1

router.post('/switch_semester',dashboardController.switch_semester);

//July 1

//Date:02/09/2021,Author: Divya

router.post('/referral_code_details', referralCodeController.referralCodeDetails)
router.post('/redeem_request', referralCodeController.redeemRequest)
router.post('/verify_referral_code', referralCodeController.verifyReferralCode)
router.post('/show_coupons', detailsController.showCoupons)

//


module.exports = router;

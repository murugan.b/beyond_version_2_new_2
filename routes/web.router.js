let express = require('express');
let router = express.Router();

let loginController = require('../controllers/web/login.controller');
let dashboardController = require('../controllers/web/dashboard.controller');
let universityController = require('../controllers/web/university.controller');
let degreeController = require('../controllers/web/degree.controller');
let semesterController = require('../controllers/web/semester.controller');
//Murugan Start
let subjectController = require('../controllers/web/subject.controller');
let subject_moduleController = require('../controllers/web/subject_module.controller');
//College
let collegeController = require('../controllers/web/college.controller');
//Setting
let settingController = require('../controllers/web/setting.controller');
//QuestionYear
let que_yearController = require('../controllers/web/que_year.controller');
//Chapter
let chapterController = require('../controllers/web/chapter.controller');

//Testing
let subject_module_fullController = require('../controllers/web/subject_module_full.controller');

//Customer and Other Details Controller
let detailsController = require('../controllers/web/details.controller');

//Edit Details Controller
let editdetailsController = require('../controllers/web/edit_details.controller');

//login
router.get('/', loginController.login);

//dashboard
router.get('/dashboard', dashboardController.dashboard);

//unveirsity listing page
router.get('/universities', universityController.page);

//new university
router.post('/university', universityController.new);

//degree listing page
router.get('/degrees', degreeController.page);

//semesters listing page
router.get('/semesters', semesterController.page);
router.get('/semester_details', semesterController.semester_details);
router.post('/semester_register', semesterController.semester_register);
//Status Update
router.get('/semester_inactive/:id', semesterController.semester_inactive);
router.get('/semester_active/:id', semesterController.semester_active);

//Murugan Start
router.post('/login_check', loginController.login_check);

router.post('/degree_register', degreeController.degree_register);

router.get('/degree_list/:id', degreeController.degree_list);

//University Status Update
router.get(
	'/university_inactive/:id',
	universityController.university_inactive
);
router.get('/university_active/:id', universityController.university_active);
//Degree Status Update
router.get('/degree_inactive/:id', degreeController.degree_inactive);
router.get('/degree_active/:id', degreeController.degree_active);

// Subject Controller
router.get('/subjects', subjectController.page);
router.post('/subject_register', subjectController.subject_register);

router.get('/subject_details', subjectController.subject_details);

router.get('/subject_inactive/:id', subjectController.subject_inactive);
router.get('/subject_active/:id', subjectController.subject_active);

//Module
router.get('/modules', subject_moduleController.page);
router.post('/module_register', subject_moduleController.module_register);
router.get('/module_details', subject_moduleController.module_details);
router.get('/module_inactive/:id', subject_moduleController.module_inactive);
router.get('/module_active/:id', subject_moduleController.module_active);
//Block and Unblocked
router.get('/module_block/:id', subject_moduleController.module_block);
router.get('/module_unblock/:id', subject_moduleController.module_unblock);

//College
router.get('/colleges', collegeController.page);
router.post('/college_register', collegeController.college_register);
router.get('/college_inactive/:id', collegeController.college_inactive);
router.get('/college_active/:id', collegeController.college_active);

//Get College List

router.get('/college_list/:id', universityController.college_list);

//router.post("/degree_list/:id", degreeController.degree_list);

//Settings
router.get('/settings', settingController.page);

router.post('/password_change', settingController.password_change);

//Question Register and Manage Details

router.get('/que_year', que_yearController.page);

router.get(
	'/quick_pre_full_details',
	que_yearController.quick_pre_full_details
);

router.post('/exam_year_register', que_yearController.exam_year_register);

//Active Status
router.get('/exam_year_inactive/:id', que_yearController.exam_year_inactive);
router.get('/exam_year_active/:id', que_yearController.exam_year_active);
//Block and Unblocked
router.get('/exam_year_block/:id', que_yearController.exam_year_block);
router.get('/exam_year_unblock/:id', que_yearController.exam_year_unblock);

//Chapter

router.get('/chapter', chapterController.page);
router.post('/content_register', chapterController.content_register);
router.get('/deep_dives_details', chapterController.deep_dives_details);
//Active Status
router.get('/content_inactive/:id', chapterController.content_inactive);
router.get('/content_active/:id', chapterController.content_active);
//Block and Unblocked
router.get('/content_block/:id', chapterController.content_block);
router.get('/content_unblock/:id', chapterController.content_unblock);

//Question Register and Manage Details

router.get('/question_page/:id/:subject', chapterController.question_page);
router.post('/question_register', chapterController.question_register);
//Active Status
router.get('/question_inactive/:id', chapterController.question_inactive);
router.get('/question_active/:id', chapterController.question_active);
//Block and Unblocked
router.get('/question_block/:id', chapterController.question_block);
router.get('/question_unblock/:id', chapterController.question_unblock);

//Sub Details Register on Question

router.get('/question_fulldetails/:id', chapterController.question_fulldetails);
router.post(
	'/question_full_register',
	chapterController.question_full_register
);

//Active Status
router.get(
	'/question_full_inactive/:id',
	chapterController.question_full_inactive
);
router.get('/question_full_active/:id', chapterController.question_full_active);
//Block and Unblocked
router.get('/question_full_block/:id', chapterController.question_full_block);
router.get(
	'/question_full_unblock/:id',
	chapterController.question_full_unblock
);
//Murugan End

//router.get("/mmm", subject_module_fullController.page);

//Details Controller
router.get('/query_details', detailsController.query_details);

router.get('/query_completed/:id', detailsController.query_completed);

router.get('/customer_details', detailsController.customer_details);

router.get('/customer_block/:id', detailsController.customer_block);

router.get('/customer_unblock/:id', detailsController.customer_unblock);

router.get(
	'/search_customer_details',
	detailsController.search_customer_details
);

router.post(
	'/search_customer_records',
	detailsController.search_customer_records
);

//Email Notify

router.get(
	'/customer_email_notify_active/:id',
	detailsController.customer_email_notify_active
);

router.get(
	'/customer_email_notify_inactive/:id',
	detailsController.customer_email_notify_inactive
);

//End Email Notify

//Voucher

router.get('/voucher', detailsController.voucher);

router.post('/voucher_register', detailsController.voucher_register);

router.get('/voucher_inactive/:id', detailsController.voucher_inactive);

router.get('/voucher_active/:id', detailsController.voucher_active);

//Purchased Couse Details on User
router.get(
	'/purchased_course_details/:id',
	detailsController.purchased_course_details
);

//Payment Procee Details
router.get(
	'/payment_process_details/:id',
	detailsController.payment_process_details
);

//Edit Details
//University
router.get('/university_edit/:id', editdetailsController.university_edit);
router.post('/university_update', editdetailsController.university_update);

//College
router.get('/college_edit/:id', editdetailsController.college_edit);
router.post('/college_update', editdetailsController.college_update);

//Degree
router.get('/degree_edit/:id', editdetailsController.degree_edit);
router.post('/degree_update', editdetailsController.degree_update);

//Semester
router.get('/semester_edit/:id', editdetailsController.semester_edit);
router.post('/semester_update', editdetailsController.semester_update);

//Subject
router.get('/subject_edit/:id', editdetailsController.subject_edit);
router.post('/subject_update', editdetailsController.subject_update);
router.get('/subject_image_edit/:id', editdetailsController.subject_image_edit);
router.post(
	'/subject_image_update',
	editdetailsController.subject_image_update
);

//Module
router.get('/module_edit/:id', editdetailsController.module_edit);
router.post('/module_update', editdetailsController.module_update);
router.get('/module_image_edit/:id', editdetailsController.module_image_edit);
router.post('/module_image_update', editdetailsController.module_image_update);

//Deep Dive Content
router.get(
	'/deep_dive_content_edit/:id',
	editdetailsController.deep_dive_content_edit
);
router.post(
	'/deep_dive_content_update',
	editdetailsController.deep_dive_content_update
);

//quick_prep_full_details_update
router.get(
	'/quick_prep_full_details_edit/:id',
	editdetailsController.quick_prep_full_details_edit
);
router.post(
	'/quick_prep_full_details_update',
	editdetailsController.quick_prep_full_details_update
);

//Question
router.get('/question_edit/:id', editdetailsController.question_edit);
router.post('/question_update', editdetailsController.question_update);

//Sub Question
router.get('/question_sub_edit/:id', editdetailsController.question_sub_edit);
router.post('/question_sub_update', editdetailsController.question_sub_update);

//web routes
router.get('/others', universityController.others);
router.post('/other_register', universityController.other_register);
router.get('/other_inactive/:id', universityController.other_inactive);
router.get('/other_active/:id', universityController.other_active);
router.get('/other_edit/:id', universityController.other_edit);
router.post('/other_update', universityController.other_update);

//May 10
//May 21
router.get(
	'/deep_dive_question_register/:id/:module_id',
	detailsController.deep_dive_question_register
);
//May 21
//deep_dive_question_register
router.post(
	'/deep_dive_question_insert',
	detailsController.deep_dive_question_insert
);
router.get(
	'/deep_question_inactive/:id',
	detailsController.deep_question_inactive
);
router.get('/deep_question_active/:id', detailsController.deep_question_active);

router.get(
	'/deep_dive_question_edit/:id',
	detailsController.deep_dive_question_edit
);
router.post(
	'/deep_dive_question_update',
	detailsController.deep_dive_question_update
);

//may12
//Deep Dive Link
//deep_dive_link_view
router.get('/deep_dive_link_view/:id', detailsController.deep_dive_link_view);
//deep_dive_link_insert
router.post('/deep_dive_link_insert', detailsController.deep_dive_link_insert);
//deep_dive_link_edit
router.get('/deep_dive_link_edit/:id', detailsController.deep_dive_link_edit);
//deep_dive_link_update
router.post('/deep_dive_link_update', detailsController.deep_dive_link_update);
//Deep Dive Link Inactive and Active
router.get(
	'/deep_dive_link_inactive/:id',
	detailsController.deep_dive_link_inactive
);
router.get(
	'/deep_dive_link_active/:id',
	detailsController.deep_dive_link_active
);

//May 18

//Deep Dive Link Inactive and Active
router.get('/deep_dive_link_lock/:id', detailsController.deep_dive_link_lock);
router.get(
	'/deep_dive_link_unlock/:id',
	detailsController.deep_dive_link_unlock
);

//July 9
router.get('/version_edit', editdetailsController.version_edit);
router.post('/version_update', editdetailsController.version_update);
//July 9

router.get(
	'/customer_email_notify_active/:id',
	detailsController.customer_email_notify_active
);
router.get(
	'/customer_email_notify_inactive/:id',
	detailsController.customer_email_notify_inactive
);

router.get(
	'/customer_email_notify_all',
	detailsController.customer_email_notify_all
);
router.get(
	'/customer_email_notify_inactive_all',
	detailsController.customer_email_notify_inactive_all
);
router.get('/hide_free_trail', detailsController.hide_free_trail);
router.get('/show_free_trail', detailsController.show_free_trail);

module.exports = router;

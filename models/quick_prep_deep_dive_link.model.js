module.exports = (sequelize, Sequelize) => {
  const QuickPrepDeepDiveLink = sequelize.define("quick_prep_deep_dive_link", {
    quick_prep_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
    },
    module_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
    },
    deep_dive_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
    },
    deep_dive_resource_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
    },
    module_topic_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
    },
  });
  return QuickPrepDeepDiveLink;
};

module.exports = (sequelize, Sequelize) => {
  const Deepdivequestion = sequelize.define("deep_dive_question", {
    question_sub_title: {
      type: Sequelize.STRING,
      defaultValue: null,
    },
    module_id: {
      type: Sequelize.INTEGER
    },
    deep_dives_id: {
      type: Sequelize.INTEGER
    },
    video_count: {
      type: Sequelize.INTEGER
    },
    pdf_count: {
      type: Sequelize.INTEGER
    },
    locked: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });
  return Deepdivequestion;
};

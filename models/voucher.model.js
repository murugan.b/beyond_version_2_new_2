module.exports = (sequelize, Sequelize) => {
  const Voucher = sequelize.define("vouchers", {
    code: {
      type: Sequelize.STRING,
    },
    type: {
      type: Sequelize.ENUM("percentage", "amount"),
      defaultValue: "percentage",
    },
    value: {
      type: Sequelize.FLOAT,
    },
    remaining_count: {
      type: Sequelize.INTEGER,
    },
    mode: {
      type: Sequelize.INTEGER,
    },
    purchase_amount: {
      type: Sequelize.FLOAT,
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
    show_in_app: {
      type: Sequelize.BOOLEAN,
      default: 1
    },
    message: {
      type: Sequelize.STRING
    }
    
  });
  return Voucher;
};

//Author: Divya,Date: 03/09/2021
module.exports = (sequelize, Sequelize) => {
    const redeem = sequelize.define("redeem", {
        customer_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        amount: {
            type: Sequelize.FLOAT(10,2),
            defaultValue: 0,
            validate: {
                min: 0
            }
        },
        date: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.DATE.Now
        },
        status: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        }
    })
    return redeem
}
// Status values 0 or 1
// 0 - Not paid
// 1 - paid

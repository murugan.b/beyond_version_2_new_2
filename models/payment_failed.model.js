module.exports = (sequelize, Sequelize) => {
    const payment_faileds = sequelize.define("payment_faileds", {
      transaction_id: {
        type: Sequelize.STRING,
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
      payment_amount: {
        type: Sequelize.INTEGER,
      },
      reason: {
        type: Sequelize.STRING,
      },
      status_payment: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    });
    return payment_faileds;
  };
  
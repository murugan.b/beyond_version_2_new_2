module.exports = (sequelize, Sequelize) => {
    const Version = sequelize.define("version", {
      name: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    });
    return Version;
  };
  
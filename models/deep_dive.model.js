module.exports = (sequelize, Sequelize) => {
  const DeepDive = sequelize.define("deep_dive", {
    module_id: {
      type: Sequelize.INTEGER
    },
    content_title: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    listing_order: {
      type: Sequelize.INTEGER,
      defaultValue: 1,
    },
    locked: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });
  return DeepDive;
};

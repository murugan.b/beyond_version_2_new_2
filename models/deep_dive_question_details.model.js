module.exports = (sequelize, Sequelize) => {
	const Deepdivequestiondetails = sequelize.define(
		'deep_dive_question_details',
		{
			question_id: {
				type: Sequelize.INTEGER,
			},
			name: {
				type: Sequelize.STRING,
				allowNull: true,
			},
			link: {
				type: Sequelize.STRING,
				allowNull: true,
			},
			type: {
				type: Sequelize.ENUM('video', 'doc', 'image'),
				defaultValue: 'video',
			},
			marque: {
				type: Sequelize.STRING,
				allowNull: true,
			},
			image_link: {
				type: Sequelize.STRING,
				allowNull: true,
			},

			description1: {
				type: Sequelize.TEXT,
				allowNull: true,
			},
			description2: {
				type: Sequelize.TEXT,
				allowNull: true,
			},

			cms_details: {
				type: Sequelize.TEXT,
				allowNull: true,
			},

			locked: {
				type: Sequelize.BOOLEAN,
				defaultValue: false,
			},
			active: {
				type: Sequelize.BOOLEAN,
				defaultValue: true,
			},
			thumbnail_link: {
				type: Sequelize.STRING,
			},
		}
	);
	return Deepdivequestiondetails;
};

module.exports = (sequelize, Sequelize) => {
    const Other = sequelize.define("other", {
      name: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    });
    return Other;
  };
  
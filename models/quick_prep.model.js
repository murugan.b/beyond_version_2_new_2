module.exports = (sequelize, Sequelize) => {
  const QuickPrep = sequelize.define("quick_prep", {
    quick_prep_index_id: {
      type: Sequelize.INTEGER
    },
    question_title: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    question_description: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    module_topic_id: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    locked: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    video_count: {
      type: Sequelize.INTEGER
    },
    pdf_count: {
      type: Sequelize.INTEGER
    },
    
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });
  return QuickPrep;
};

module.exports = (sequelize, Sequelize) => {
    const Verification = sequelize.define("verification", {
      mobile: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      otp: {
        type: Sequelize.INTEGER
      },

      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    });
    return Verification;
  };
  
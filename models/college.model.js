module.exports = (sequelize, Sequelize) => {
    const College = sequelize.define("college", {
      name: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      university_id: {
        type: Sequelize.INTEGER
      },

      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
    });
    return College;
  };
  
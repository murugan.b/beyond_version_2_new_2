module.exports = (sequelize, Sequelize) => {
  const QuickPrepIndex = sequelize.define("quick_prep_index", {
    exam_year: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    exam_month: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    listing_order: {
      type: Sequelize.INTEGER,
      defaultValue: 1,
    },
    subject_id: {
      type: Sequelize.INTEGER
    },
    download_link: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    locked: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });
  return QuickPrepIndex;
};

module.exports = (sequelize, Sequelize) => {
    const global_settings = sequelize.define("global_settings", {
        key: {
            type: Sequelize.STRING
        },
        value: {
            type: Sequelize.STRING
        }
    })
    return global_settings
}
// otp_bypass values
// 0 - send otp by SMS
// 1 - send otp bt email
// 2 - Bypass both sms and email
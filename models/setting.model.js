module.exports = (sequelize, Sequelize) => {
    const Setting = sequelize.define("admins", {
        full_name: {
            type: Sequelize.STRING(50),
            allowNull: true,
          },
          phone: {
            type: Sequelize.STRING(50),
            allowNull: true,
          },
          email: {
            type: Sequelize.STRING(100),
            allowNull: true,
          },
          password: {
            type: Sequelize.STRING(60),
            allowNull: true,
          },
          super_admin: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
          },
          delete_status: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
          },
          active: {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
          },
    
    });
    return Setting;
  };
  
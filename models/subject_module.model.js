module.exports = (sequelize, Sequelize) => {
  const Module = sequelize.define("module", {
    name: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    listing_order: {
      type: Sequelize.INTEGER,
      defaultValue: 1,
    },
    subject_id: {
      type: Sequelize.INTEGER
    },
    locked: {
      type: Sequelize.INTEGER
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });

  //Module.belongsTo(Subject, {foreignKey: 'id'});
  //Post.find({ where: {}, include: [Module]});

  return Module;
};

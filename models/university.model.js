module.exports = (sequelize, Sequelize) => {
  const University = sequelize.define("university", {
    name: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });
  return University;
};

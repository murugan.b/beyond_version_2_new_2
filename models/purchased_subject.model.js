module.exports = (sequelize, Sequelize) => {
  const PurchasedCourse = sequelize.define("purchased_courses", {
    user_id: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    subject_id: {
      type: Sequelize.INTEGER,
    },
    payment_transaction_id: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    created_at: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
  });
  return PurchasedCourse;
};

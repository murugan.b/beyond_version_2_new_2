module.exports = (sequelize, Sequelize) => {
  const Customer = sequelize.define("customer", {
    full_name: {
      type: Sequelize.STRING,
    },
    phone: {
      type: Sequelize.STRING,
    },
    email: {
      type: Sequelize.STRING,
    },
    image: {
      type: Sequelize.STRING,
    },
    university_id: {
      type: Sequelize.INTEGER,
    },
    college_id: {
      type: Sequelize.INTEGER,
    },
    degree_id: {
      type: Sequelize.INTEGER,
    },
    semester_id: {
      type: Sequelize.INTEGER,
    },
    api_token: {
      type: Sequelize.STRING(60),
      allowNull: true,
    },

    delete_status: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },

    email_notify: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
    },
    
    expire_date: {
      type: Sequelize.INTEGER,
    },
    expire_hrs: {
      type: Sequelize.INTEGER,
    },
    expire_mins: {
      type: Sequelize.INTEGER,
    },
    expire_month: {
      type: Sequelize.INTEGER,
    },
    enable_free_trial: {
      type: Sequelize.INTEGER,
      defaultValue: 1,
    },
    otp_bypass: {
      type: Sequelize.ENUM('0','1','2'),
      default: 0,
    },
    global_key: {
      type: Sequelize.INTEGER,
      defaultValue: 4
    },
    referral_code: {
      type: Sequelize.STRING
    },
    total_amount: {
      type: Sequelize.FLOAT,
      defaultValue: 0
    },
    balance_amount: {
      type: Sequelize.FLOAT,
      defaultValue: 0
    },
    referral_value: {
      type: Sequelize.INTEGER,
      default: 5
    }
  });
  // Customer.associate = function (models) {
  //   Customer.hasMany(models.free_trial,{})
  // }
  return Customer;
};

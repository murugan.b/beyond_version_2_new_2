module.exports = (sequelize, Sequelize) => {
  const DeepDiveResource = sequelize.define("deep_dive_resource", {
    content_sub_title: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    deep_dive_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
    },
    listing_order: {
      type: Sequelize.INTEGER,
      defaultValue: 1,
    },
    link: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    type: {
      type: Sequelize.ENUM("video", "doc"),
      defaultValue: "video",
    },
    locked: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });
  return DeepDiveResource;
};

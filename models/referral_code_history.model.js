// Author: Divya, Date: 03/09/2021
module.exports = (sequelize, Sequelize) => {
    const referralCodeHistory = sequelize.define("referral_code_history", {
        customer_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        date: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.Now
        },
        amount: {
            type: Sequelize.FLOAT(10,2),
            defaultValue: 0,
            validate: {
                min: 0
            }
        },
        referral_code: {
            type: Sequelize.STRING
        },
        used_by: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    })
    return referralCodeHistory
}
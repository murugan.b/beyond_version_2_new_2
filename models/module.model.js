module.exports = (sequelize, Sequelize) => {
  const Module = sequelize.define("module", {
    name: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    listing_order: {
      type: Sequelize.INTEGER,
      defaultValue: 1,
    },
    subject_id: {
      type: Sequelize.INTEGER
    },
    image: {
      type: Sequelize.STRING,
    },
    locked: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });
  return Module;
};

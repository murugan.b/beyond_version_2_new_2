module.exports = (sequelize,Sequelize) => {
    const freeTrial = sequelize.define("free_trial", {
        customer_id: {
            type: Sequelize.INTEGER
        },
        semester_id: {
            type: Sequelize.INTEGER
        },
        start_time: {
            type: Sequelize.DATE
        },
        end_time: {
            type: Sequelize.DATE
        },
        cron_time: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        },
        global_key: {
            type: Sequelize.INTEGER,
            defaultValue: 2
        }
    })
    // freeTrial.associate = function (models) {
    //     models.free_trial.belongsTo(models.customer,{
    //         foreignKey: 'customer_id'
    //     })
    // }
    return freeTrial
}
// status field represents free trial status
// 0 - Not activated
// 1 - Activated
// 3 - Expired
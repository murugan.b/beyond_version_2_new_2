module.exports = (sequelize, Sequelize) => {
  const Subject = sequelize.define("subject", {
    name: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    price: {
      type: Sequelize.INTEGER,
   
    },

    semester_id: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    image: {
      type: Sequelize.STRING,
    },
    
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });


//  Subject.hasMany(module, {foreignKey: 'id'});



  return Subject;
};

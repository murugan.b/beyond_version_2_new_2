module.exports = (sequelize, Sequelize) => {
    const Voucher_user = sequelize.define("voucher_users", {
      code: {
        type: Sequelize.STRING,
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
    });
    return Voucher_user;
  };
  
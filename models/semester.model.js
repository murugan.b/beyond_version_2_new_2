module.exports = (sequelize, Sequelize) => {
  const Semester = sequelize.define("semester", {
    name: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    degree_id: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
    enable_free_trial: {
      type: Sequelize.INTEGER,
      default: 1
    },
    global_key: {
      type: Sequelize.INTEGER,
      defaultValue: 4
    }
  });
  return Semester;
};
// free trial values
// 0 - Not activated
// 1 - Activated
module.exports = (sequelize, Sequelize) => {
  const PurchasedCourse = sequelize.define("payment_transactions", {
    transaction_id: {
      type: Sequelize.STRING,
    },
    user_id: {
      type: Sequelize.INTEGER,
    },
    voucher_id: {
      type: Sequelize.INTEGER,
    },
    payment_amount: {
      type: Sequelize.INTEGER,
    },
    transaction_meta: {
      type: Sequelize.STRING,
    },
    created_at: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
  });
  return PurchasedCourse;
};

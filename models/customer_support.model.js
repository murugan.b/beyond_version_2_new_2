module.exports = (sequelize, Sequelize) => {
    const Customer_support = sequelize.define("customer_support", {
        customer_id: {
          type: Sequelize.INTEGER,
          },
          message: {
            type: Sequelize.STRING,
          },
          
          status: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
          },
          active: {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
          },
    
    });
    return Customer_support;
  };
  
module.exports = (sequelize, Sequelize) => {
  const QuickPrepResource = sequelize.define("quick_prep_resource", {
    question_sub_title: {
      type: Sequelize.STRING,
      defaultValue: null,
    },
    quick_prep_id: {
      type: Sequelize.INTEGER
    },
    link: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    type: {
      type: Sequelize.ENUM("video", "doc","image"),
      defaultValue: "video",
    },
    marque: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    image_link: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    description1: {
      type: Sequelize.TEXT,
      allowNull: true,
    },
    description2: {
      type: Sequelize.TEXT,
      allowNull: true,
    },
    cms_details: {
      type: Sequelize.TEXT,
      allowNull: true,
    },
    locked: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });
  return QuickPrepResource;
};

const config = require('config');

const Sequelize = require('sequelize');
const sequelize = new Sequelize('beyondtheory', 'root', 'beyondTheSky567#', {
	host: 'localhost',
	dialect: 'mysql',
	operatorsAliases: '0',
	pool: {
		max: 5,
		min: 0,
		acquire: 30000,
		idle: 10000,
	},
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.admins = require('./admin.model.js')(sequelize, Sequelize);
db.customers = require('./customer.model.js')(sequelize, Sequelize);
db.deep_dive = require('./deep_dive.model.js')(sequelize, Sequelize);
db.degree = require('./degree.model.js')(sequelize, Sequelize);
db.module = require('./module.model.js')(sequelize, Sequelize);

//Author: Divya,Date: August2021
db.global_settings = require('./global_settings.model')(sequelize, Sequelize);
db.free_trial = require('./free_trial.model.js')(sequelize, Sequelize);
db.referral_code_history = require('./referral_code_history.model')(
	sequelize,
	Sequelize
);
db.redeem = require('./redeem.model')(sequelize, Sequelize);
//end

db.payment_faileds = require('./payment_failed.model.js')(sequelize, Sequelize);

//JULY 9
db.version = require('./version.model.js')(sequelize, Sequelize);
//JULY 9

//Jun 10
db.voucher_user = require('./voucher_user.model.js')(sequelize, Sequelize);
//Jun 10

db.register_count_details = require('./register_count_details.model.js')(
	sequelize,
	Sequelize
);

//other's
db.other = require('./other.model.js')(sequelize, Sequelize);

//Verification
db.verification = require('./verification.model.js')(sequelize, Sequelize);

////May 10  Deep Dive Question

db.deep_dive_question = require('./deep_dive_question.model.js')(
	sequelize,
	Sequelize
);
//May 12
db.deep_dive_question_details =
	require('./deep_dive_question_details.model.js')(sequelize, Sequelize);

db.quick_prep_deep_dive_link = require('./quick_prep_deep_dive_link.model.js')(
	sequelize,
	Sequelize
);
db.quick_prep_index = require('./quick_prep_index.model.js')(
	sequelize,
	Sequelize
);
db.quick_prep_resource = require('./quick_prep_resource.model.js')(
	sequelize,
	Sequelize
);
db.quick_prep = require('./quick_prep.model.js')(sequelize, Sequelize);
db.semester = require('./semester.model.js')(sequelize, Sequelize);
db.subject = require('./subject.model.js')(sequelize, Sequelize);
db.university = require('./university.model.js')(sequelize, Sequelize);

//New Tables Start

db.payment_transaction = require('./payment_transaction.model')(
	sequelize,
	Sequelize
);
db.purchased_subject = require('./purchased_subject.model')(
	sequelize,
	Sequelize
);
db.voucher = require('./voucher.model')(sequelize, Sequelize);

//New Table End

//Murugan Start

db.modules = require('./subject_module.model.js')(sequelize, Sequelize);

db.degree = require('./degree.model.js')(sequelize, Sequelize);

db.college = require('./college.model.js')(sequelize, Sequelize);

db.customer_support = require('./customer_support.model.js')(
	sequelize,
	Sequelize
);

db.setting = require('./setting.model.js')(sequelize, Sequelize);

//Relations
//db.university.belongsTo(db.degree);

//Subject Modules Relationship

db.module.belongsTo(db.subject, { foreignKey: 'subject_id', targetKey: 'id' });
db.subject.hasOne(db.module, { foreignKey: 'subject_id', targetKey: 'id' });

// //University and College Relationship

db.college.belongsTo(db.university, {
	foreignKey: 'university_id',
	targetKey: 'id',
});
db.university.hasOne(db.college, {
	foreignKey: 'university_id',
	targetKey: 'id',
});

// //College and Degree Relationship

//Now Change
db.degree.belongsTo(db.college, { foreignKey: 'college_id', targetKey: 'id' });
db.college.hasOne(db.degree, { foreignKey: 'college_id', targetKey: 'id' });

//Now Change

//New University and Degree Relationship

db.degree.belongsTo(db.university, {
	foreignKey: 'university_id',
	targetKey: 'id',
});
db.university.hasOne(db.degree, {
	foreignKey: 'university_id',
	targetKey: 'id',
});

//Subject  and Exam Years  and Month Relationship

db.subject.hasOne(db.quick_prep_index, {
	foreignKey: 'subject_id',
	targetKey: 'id',
});

db.quick_prep_index.belongsTo(db.subject, {
	foreignKey: 'subject_id',
	targetKey: 'id',
});

//college->subject
//degree-> quick_prep_index

// Module and Content Relation

db.module.hasOne(db.deep_dive, { foreignKey: 'module_id', targetKey: 'id' });

db.deep_dive.belongsTo(db.module, { foreignKey: 'module_id', targetKey: 'id' });

//subject->Module
//quick_prep_index-> deep_dive

//Semester  and Degree, Degree and College Relationship

//college=>semster, university=>degree
db.semester.belongsTo(db.degree, { foreignKey: 'degree_id', targetKey: 'id' });
db.degree.hasOne(db.semester, { foreignKey: 'degree_id', targetKey: 'id' });

//Semester  and Subject Relationship

db.subject.belongsTo(db.semester, {
	foreignKey: 'semester_id',
	targetKey: 'id',
});
db.semester.hasOne(db.subject, { foreignKey: 'semester_id', targetKey: 'id' });

//Subject Module Relationship

db.module.belongsTo(db.subject, { foreignKey: 'subject_id', targetKey: 'id' });
db.subject.hasOne(db.module, { foreignKey: 'subject_id', targetKey: 'id' });

//db.degree.belongsTo(db.college);
//db.posts.hasMany(db.degree);

//Quick Prep and Qucik prep deep dive links

db.quick_prep_deep_dive_link.belongsTo(db.quick_prep, {
	foreignKey: 'quick_prep_id',
	targetKey: 'id',
});
db.quick_prep.hasOne(db.quick_prep_deep_dive_link, {
	foreignKey: 'quick_prep_id',
	targetKey: 'id',
});

//Quick Prep and Quick Prep Resource

db.quick_prep_resource.belongsTo(db.quick_prep, {
	foreignKey: 'quick_prep_id',
	targetKey: 'id',
});
db.quick_prep.hasOne(db.quick_prep_resource, {
	foreignKey: 'quick_prep_id',
	targetKey: 'id',
});

// Customer and Query Management Details

db.customer_support.belongsTo(db.customers, {
	foreignKey: 'customer_id',
	targetKey: 'id',
});
db.customers.hasOne(db.customer_support, {
	foreignKey: 'customer_id',
	targetKey: 'id',
});

//Semester and Customer Relationship

db.customers.belongsTo(db.semester, {
	foreignKey: 'semester_id',
	targetKey: 'id',
});
db.semester.hasOne(db.customers, {
	foreignKey: 'semester_id',
	targetKey: 'id',
});

// Quick Prep Index  and Qucik Prep Relation quick_prep_index

// db.quick_prep_index.belongsTo(db.quick_prep, {foreignKey: 'quick_prep_id', targetKey: 'id'});
// db.quick_prep.hasOne(db.quick_prep_index, {foreignKey: 'quick_prep_id', targetKey: 'id'});

db.quick_prep_index.belongsTo(db.quick_prep, {
	foreignKey: 'quick_prep_id',
	targetKey: 'id',
});
db.quick_prep.hasOne(db.quick_prep_index, {
	foreignKey: 'quick_prep_id',
	targetKey: 'id',
});

//Module and Quick Prep Deep Dive Link Relationship

db.quick_prep_deep_dive_link.belongsTo(db.module, {
	foreignKey: 'module_id',
	targetKey: 'id',
});
db.module.hasOne(db.quick_prep_deep_dive_link, {
	foreignKey: 'module_id',
	targetKey: 'id',
});

//Purchased Subject and Subject Details

db.purchased_subject.belongsTo(db.subject, {
	foreignKey: 'subject_id',
	targetKey: 'id',
});
db.subject.hasOne(db.purchased_subject, {
	foreignKey: 'subject_id',
	targetKey: 'id',
});

//May 22

db.deep_dive_question.belongsTo(db.module, {
	foreignKey: 'module_id',
	targetKey: 'id',
});
db.module.hasOne(db.deep_dive_question, {
	foreignKey: 'module_id',
	targetKey: 'id',
});

//May 22

//Murugan End

//September divya

//customer and referral_code_history relationship

db.referral_code_history.belongsTo(db.customers, {
	foreignKey: 'customer_id',
	targetKey: 'id',
});
db.customers.hasOne(db.referral_code_history, {
	foreignKey: 'customer_id',
	targetKey: 'id',
});

db.referral_code_history.belongsTo(db.customers, {
	foreignKey: 'used_by',
	targetKey: 'id',
});
db.customers.hasOne(db.referral_code_history, {
	foreignKey: 'used_by',
	targetKey: 'id',
});

// redeem and customer table relation

db.redeem.belongsTo(db.customers, {
	foreignKey: 'customer_id',
	targetKey: 'id',
});
db.customers.hasOne(db.redeem, { foreignKey: 'customer_id', targetKey: 'id' });

// global_setting and customer table relation

db.customers.belongsTo(db.global_settings, {
	foreignKey: 'global_key',
	targetKey: 'id',
});
db.global_settings.hasOne(db.customers, {
	foreignKey: 'global_key',
	targetKey: 'id',
});

db.customers.belongsTo(db.global_settings, {
	foreignKey: 'referral_value',
	targetKey: 'id',
});
db.global_settings.hasOne(db.customers, {
	foreignKey: 'referral_value',
	targetKey: 'id',
});

// global_setting and semester table relation

db.semester.belongsTo(db.global_settings, {
	foreignKey: 'global_key',
	targetKey: 'id',
});
db.global_settings.hasOne(db.semester, {
	foreignKey: 'global_key',
	targetKey: 'id',
});

// global_setting and free_trial table relation

db.free_trial.belongsTo(db.global_settings, {
	foreignKey: 'global_key',
	targetKey: 'id',
});
db.global_settings.hasOne(db.free_trial, {
	foreignKey: 'global_key',
	targetKey: 'id',
});

// customer and free_trial table relation

db.free_trial.belongsTo(db.customers, {
	foreignKey: 'customer_id',
	targetKey: 'id',
});
db.customers.hasOne(db.free_trial, {
	foreignKey: 'customer_id',
	targetKey: 'id',
});

//end divya

module.exports = db;

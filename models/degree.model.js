module.exports = (sequelize, Sequelize) => {
  const Degree = sequelize.define("degree", {
    name: {
      type: Sequelize.STRING,
    },
   
    university_id: {
      type: Sequelize.INTEGER,
    },
   
   
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });
  return Degree;
  

  
};

-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2020 at 06:39 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beyondtheory`
--

-- --------------------------------------------------------

--
-- Table structure for table `global_settings`
--

CREATE TABLE `global_settings` (
  `id` int(11) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `free_trial`
--

CREATE TABLE `free_trial` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `start_time` DATE NOT NULL,
  `end_time` DATE NOT NULL,
  `cron_time` varchar(255) NOT NULL,
  `global_key` int(11) DEFAULT 2,
  `status` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Table structure for table `redeem`
--

CREATE TABLE `redeem` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` FLOAT NOT NULL,
  `date` DATE NOT NULL,
  `status` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `referral_code_history`
--

CREATE TABLE `referral_code_history` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` FLOAT NOT NULL,
  `date` DATE NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `used_by` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Dumping data for table `global_settings`
--

INSERT INTO `global_settings`(`id`, `key`, `value`, `createdAt`, `updatedAt`) VALUES (1,'otp_bypass',0,'2021-08-22 07:07:12','2021-08-22 07:07:12');
INSERT INTO `global_settings`(`id`, `key`, `value`, `createdAt`, `updatedAt`) VALUES (2,'fhr_activated_msg','Free trial activated successfully','2020-04-22 07:07:12','2020-04-22 07:07:12');
INSERT INTO `global_settings`(`id`, `key`, `value`, `createdAt`, `updatedAt`) VALUES (3,'fhr_expired_msg','Sorry...You have already taken your free trial!!!','2020-04-22 07:07:12','2020-04-22 07:07:12');
INSERT INTO `global_settings`(`id`, `key`, `value`, `createdAt`, `updatedAt`) VALUES (4,'fhr_notavailable_msg','Sorry....Free trial is not available','2020-04-22 07:07:12','2020-04-22 07:07:12');
INSERT INTO `global_settings` (`id`, `key`, `createdAt`, `updatedAt`, `value`) VALUES (5, 'referral_value', '2020-04-22 07:07:12', '2020-04-22 07:07:12', 15);
INSERT INTO `global_settings` (`id`, `key`, `value`, `createdAt`, `updatedAt`) VALUES (6, 'fhr_already_activated_msg', 'Already activated', '2021-09-16 11:24:44', '2021-09-16 11:24:44');

-- --------------------------------------------------------

--
-- Dumping data for table `free_trial`
--

INSERT INTO `free_trial`(`id`, `customer_id`, `semester_id`, `start_time`, `end_time`, `cron_time`, `global_key`, `status`, `createdAt`, `updatedAt`) 
VALUES (1, 1, 1,'2021-08-30 14:57:13','2021-08-31 14:57:13', '27 20 31 Aug 2', 4, 0);
INSERT INTO `free_trial`(`id`, `customer_id`, `semester_id`, `start_time`, `end_time`, `cron_time`, `global_key`, `status`, `createdAt`, `updatedAt`) 
VALUES (1, 1, 1,'2021-08-30 14:57:13','2021-08-31 14:57:13', '27 20 31 Aug 2', 2, 1);
INSERT INTO `free_trial`(`id`, `customer_id`, `semester_id`, `start_time`, `end_time`, `cron_time`, `global_key`, `status`, `createdAt`, `updatedAt`) 
VALUES (2, 2, 2,'2021-08-30 14:57:13','2021-08-31 14:57:13', '27 20 31 Aug 2', 3, 2);


-- --------------------------------------------------------

--
-- Dumping data for table `redeem`
--

INSERT INTO `redeem`(`id`, `customer_id`, `amount`, `date`, `status`, `createdAt`, `updatedAt`) 
VALUES (1, 1, 100.00, '2021-08-22 07:07:12', 0, '2021-08-22 07:07:12','2021-08-22 07:07:12');

-- --------------------------------------------------------

--
-- Dumping data for table `referral_code_history`
--

INSERT INTO `referral_code_history`(`id`, `customer_id`, `amount`, `date`, `referral_code`, `used_by`, `createdAt`, `updatedAt`) 
VALUES (1, 1, 100.00, '2021-08-22 07:07:12', 'BT_customername1234', 2, '2021-08-22 07:07:12','2021-08-22 07:07:12');

-- --------------------------------------------------------


--
-- Indexes for table `global_settings`
--
ALTER TABLE `global_settings`
  ADD PRIMARY KEY (`id`);


--
-- Indexes for table `free_trial`
--
ALTER TABLE `free_trial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `redeem`
--
ALTER TABLE `redeem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_code_history`
--
ALTER TABLE `referral_code_history`
  ADD PRIMARY KEY (`id`);
 
-------------------------------------------------------------- 
--
-- AUTO_INCREMENT for table `global_settings`
--
ALTER TABLE `global_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `free_trial`
--
ALTER TABLE `free_trial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;  

--
-- AUTO_INCREMENT for table `redeem`
--
ALTER TABLE `redeem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT; 

--
-- AUTO_INCREMENT for table `referral_code_history`
--
ALTER TABLE `referral_code_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;  

----------------------------------------------------------

 --
 --ALTER table 'customer` to add new columns
 --
ALTER TABLE `customer` ADD COLUMN 'total_amount' FLOAT DEFAULT 0
ALTER TABLE `customer` ADD COLUMN 'balance_amount' FLOAT DEFAULT 0
ALTER TABLE `customer` ADD COLUMN 'global_key' int DEFAULT 4
ALTER TABLE `customer` ADD COLUMN 'otp_bypass' int DEFAULT 0
ALTER TABLE `customer` ADD COLUMN 'referral_code' VARCHAR(100)
ALTER TABLE `customer` ADD COLUMN 'referral_value' int DEFAULT 5

 --
 --ALTER table 'semester` to add new columns
 --

ALTER TABLE `semester` ADD COLUMN 'global_key' int DEFAULT 4
ALTER TABLE `semester` ADD COLUMN 'enable_free_trial' int DEFAULT 1

-----------------------------------------------------------


--
 --ALTER table 'voucher` to add new columns
 --

ALTER TABLE `voucher` ADD COLUMN 'show_in_app' BOOLEAN DEFAULT 1
ALTER TABLE `voucher` ADD COLUMN 'message' VARCHAR(255)

-----------------------------------------------------------



----------RENAME--------------------------------------------
--
 --ALTER table 'customer` to rename free_trial column
 --

ALTER TABLE `customer` RENAME COLUMN `free_trial` TO `enable_free_trial`


-----------------------------------------------------------


COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

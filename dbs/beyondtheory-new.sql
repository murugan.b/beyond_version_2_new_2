-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2020 at 06:39 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beyondtheory`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `super_admin` tinyint(1) DEFAULT 0,
  `delete_status` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `full_name`, `phone`, `email`, `password`, `super_admin`, `delete_status`, `active`, `createdAt`, `updatedAt`) VALUES
(1, NULL, NULL, 'admin@gmail.com', 'sha1$8eb1752c$1$d9dda4a6570b4709f779f8b8eb7b1426a556e8ec', 0, 0, 1, '0000-00-00 00:00:00', '2020-04-28 11:39:16');

-- --------------------------------------------------------

--
-- Table structure for table `colleges`
--

CREATE TABLE `colleges` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `university_id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `colleges`
--

INSERT INTO `colleges` (`id`, `name`, `university_id`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 'PSYEC', 1, 1, '0000-00-00 00:00:00', '2020-04-28 10:13:25'),
(2, 'Chennai Anna', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'SIT', 2, 1, '2020-04-17 06:56:29', '2020-04-28 04:04:23'),
(4, 'Kamarajar Engineering College', 1, 1, '2020-04-17 06:56:48', '2020-04-17 07:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `university_id` int(11) DEFAULT NULL,
  `degree_id` int(11) DEFAULT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `api_token` varchar(60) DEFAULT NULL,
  `delete_status` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `full_name`, `phone`, `email`, `university_id`, `degree_id`, `semester_id`, `api_token`, `delete_status`, `active`, `createdAt`, `updatedAt`) VALUES
(1, ' Murugan-test2', '8056341625', 'ersaravana386@gmail.com', 1, 1, 1, '5aab49fd-0e48-5047-8978-305c4c43c86f', 0, 1, '2020-04-23 07:08:46', '2020-04-28 10:14:51'),
(2, 'Murugan', '8610363201', 'ersaravana386@gmail.com', 1, 1, 1, NULL, 0, 1, '2020-04-28 05:27:07', '2020-04-28 05:27:07');

-- --------------------------------------------------------

--
-- Table structure for table `customer_supports`
--

CREATE TABLE `customer_supports` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_supports`
--

INSERT INTO `customer_supports` (`id`, `customer_id`, `message`, `status`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Customer Some Detailed Message', 1, 1, '2020-04-25 04:14:57', '2020-04-25 05:53:39'),
(2, 1, 'Test Message Details', 0, 1, '2020-04-28 06:30:48', '2020-04-28 06:30:48');

-- --------------------------------------------------------

--
-- Table structure for table `deep_dives`
--

CREATE TABLE `deep_dives` (
  `id` int(11) NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `content_title` varchar(255) DEFAULT NULL,
  `listing_order` int(11) DEFAULT 1,
  `locked` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `deep_dives`
--

INSERT INTO `deep_dives` (`id`, `module_id`, `content_title`, `listing_order`, `locked`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 2, 'Tamil', 1, 0, 1, '2020-04-22 08:37:39', '2020-04-28 04:06:33'),
(2, 4, 'testtest', 1, 0, 1, '2020-04-22 08:38:46', '2020-04-22 10:12:55'),
(3, 1, 'jjjjjjjjjjjjjjjjjjj', 1, 0, 1, '2020-04-22 11:20:51', '2020-04-22 11:20:51');

-- --------------------------------------------------------

--
-- Table structure for table `degrees`
--

CREATE TABLE `degrees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `college_id` int(11) DEFAULT NULL,
  `image` text NOT NULL,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `university_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `degrees`
--

INSERT INTO `degrees` (`id`, `name`, `college_id`, `image`, `active`, `createdAt`, `updatedAt`, `university_id`) VALUES
(1, 'BE-CSE', 1, '1587987560056civil.png', 1, '2020-04-17 11:58:37', '2020-04-27 08:14:18', 0),
(2, 'BE-ECE', 2, '1587987560056civil.png', 1, '2020-04-17 11:58:57', '2020-04-28 04:04:29', 0),
(3, 'BE-IT', 3, '1587987560056civil.png', 1, '2020-04-17 11:59:19', '2020-04-17 11:59:19', 0),
(4, 'BE-MECH', 1, '1587987560056civil.png', 1, '2020-04-17 11:59:39', '2020-04-17 11:59:39', 0),
(5, 'BE-ECE', 1, '1587987560056civil.png', 1, '2020-04-25 11:22:18', '2020-04-25 11:22:18', 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` text NOT NULL,
  `listing_order` int(11) DEFAULT 1,
  `subject_id` int(11) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `image`, `listing_order`, `subject_id`, `locked`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 'english1', '1587991007431module.jpg', 1, 2, 0, 1, '2020-04-22 07:07:12', '2020-04-28 04:06:23'),
(2, 'Tamil1', '1587991007431module.jpg', 1, 1, 0, 1, '2020-04-22 07:07:43', '2020-04-22 09:00:43'),
(3, 'FOC1', '1587991007431module.jpg', 1, 3, 0, 1, '2020-04-22 07:07:49', '2020-04-28 04:06:22'),
(4, 'Foc5', '1587991007431module.jpg', 1, 3, 0, 1, '2020-04-22 07:32:09', '2020-04-22 09:00:40');

-- --------------------------------------------------------

--
-- Table structure for table `payment_transactions`
--

CREATE TABLE `payment_transactions` (
  `id` int(11) NOT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `transaction_meta` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment_transactions`
--

INSERT INTO `payment_transactions` (`id`, `transaction_id`, `user_id`, `voucher_id`, `transaction_meta`, `created_at`, `createdAt`, `updatedAt`) VALUES
(1, '121212kj1kllkjsdljkflsdjf', 1, 1, 'asdssssssssssfff', '2020-04-25 12:33:42', '2020-04-25 12:33:42', '2020-04-25 12:33:42');

-- --------------------------------------------------------

--
-- Table structure for table `purchased_courses`
--

CREATE TABLE `purchased_courses` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `payment_transaction_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchased_courses`
--

INSERT INTO `purchased_courses` (`id`, `user_id`, `subject_id`, `payment_transaction_id`, `created_at`, `createdAt`, `updatedAt`) VALUES
(1, '1', 1, 121212, '2020-04-25 12:21:59', '2020-04-25 12:21:59', '2020-04-25 12:21:59');

-- --------------------------------------------------------

--
-- Table structure for table `quick_preps`
--

CREATE TABLE `quick_preps` (
  `id` int(11) NOT NULL,
  `quick_prep_index_id` int(11) DEFAULT NULL,
  `question_title` varchar(255) DEFAULT NULL,
  `question_description` varchar(255) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quick_preps`
--

INSERT INTO `quick_preps` (`id`, `quick_prep_index_id`, `question_title`, `question_description`, `locked`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Tamil 1 Questions', 'Sample Tamil', 0, 1, '2020-04-22 11:12:06', '2020-04-28 04:31:47'),
(2, 4, 'english1 question', 'sdddddddddddd', 0, 1, '2020-04-22 11:21:57', '2020-04-22 11:21:57');

-- --------------------------------------------------------

--
-- Table structure for table `quick_prep_deep_dive_links`
--

CREATE TABLE `quick_prep_deep_dive_links` (
  `id` int(11) NOT NULL,
  `quick_prep_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `deep_dive_id` int(11) DEFAULT NULL,
  `deep_dive_resource_id` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quick_prep_deep_dive_links`
--

INSERT INTO `quick_prep_deep_dive_links` (`id`, `quick_prep_id`, `module_id`, `deep_dive_id`, `deep_dive_resource_id`, `createdAt`, `updatedAt`) VALUES
(1, 1, 2, 1, NULL, '2020-04-22 11:12:06', '2020-04-22 11:12:06'),
(2, 4, 1, 3, NULL, '2020-04-22 11:21:57', '2020-04-22 11:21:57');

-- --------------------------------------------------------

--
-- Table structure for table `quick_prep_indices`
--

CREATE TABLE `quick_prep_indices` (
  `id` int(11) NOT NULL,
  `exam_year` varchar(255) DEFAULT NULL,
  `exam_month` varchar(255) DEFAULT NULL,
  `listing_order` int(11) DEFAULT 1,
  `subject_id` int(11) DEFAULT NULL,
  `download_link` varchar(255) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quick_prep_indices`
--

INSERT INTO `quick_prep_indices` (`id`, `exam_year`, `exam_month`, `listing_order`, `subject_id`, `download_link`, `locked`, `active`, `createdAt`, `updatedAt`) VALUES
(1, '2008', 'Jan', 1, 1, 'http://www.africau.edu/images/default/sample.pdf', 0, 1, '2020-04-22 10:25:53', '2020-04-28 04:30:31'),
(2, '2010', 'Dec', 1, 3, 'http://www.africau.edu/images/default/sample.pdf', 0, 1, '2020-04-22 10:29:30', '2020-04-22 10:54:08'),
(3, '2020', 'Feb', 1, 2, 'http://www.africau.edu/images/default/sample.pdf', 0, 1, '2020-04-22 11:20:18', '2020-04-28 04:30:30'),
(4, '2021', 'jan', 1, 2, 'http://www.africau.edu/images/default/sample.pdf', 0, 1, '2020-04-22 11:21:07', '2020-04-22 11:21:07');

-- --------------------------------------------------------

--
-- Table structure for table `quick_prep_resources`
--

CREATE TABLE `quick_prep_resources` (
  `id` int(11) NOT NULL,
  `question_sub_title` varchar(255) DEFAULT NULL,
  `quick_prep_id` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `type` enum('video','doc') DEFAULT 'video',
  `locked` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quick_prep_resources`
--

INSERT INTO `quick_prep_resources` (`id`, `question_sub_title`, `quick_prep_id`, `link`, `type`, `locked`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 'Tamil Question one ', 1, 'http://www.pdf995.com/samples/pdf.pdf', 'doc', 0, 1, '2020-04-22 11:13:54', '2020-04-28 04:33:47');

-- --------------------------------------------------------

--
-- Table structure for table `register_count_details`
--

CREATE TABLE `register_count_details` (
  `id` int(11) NOT NULL,
  `quick_prep_resource_id` int(11) DEFAULT NULL,
  `quick_prep_id` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `register_count_details`
--

INSERT INTO `register_count_details` (`id`, `quick_prep_resource_id`, `quick_prep_id`, `createdAt`, `updatedAt`) VALUES
(1, 1, 1, '2020-04-29 05:55:06', '2020-04-29 05:55:06');

-- --------------------------------------------------------

--
-- Table structure for table `semesters`
--

CREATE TABLE `semesters` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `degree_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `semesters`
--

INSERT INTO `semesters` (`id`, `name`, `degree_id`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 'semester1', 1, 1, '2020-04-22 06:28:17', '2020-04-28 04:04:37'),
(2, 'Semester1 ', 4, 1, '2020-04-22 06:29:53', '2020-04-22 06:32:17'),
(3, 'Semester5', 3, 1, '2020-04-22 06:31:17', '2020-04-22 06:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` int(5) NOT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `price`, `semester_id`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 'Tamil', 100, 1, 1, '2020-04-22 06:32:34', '2020-04-28 04:05:51'),
(2, 'English', 200, 3, 1, '2020-04-22 06:32:54', '2020-04-22 06:33:30'),
(3, 'FOC', 300, 3, 1, '2020-04-22 06:33:05', '2020-04-22 06:33:32'),
(13, 'Test Semester for CSEPSYECSEM1', 500, 1, 1, '2020-04-25 10:02:34', '2020-04-25 10:03:43');

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE `universities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `universities`
--

INSERT INTO `universities` (`id`, `name`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 'Anna University', 1, '2020-04-17 06:23:07', '2020-04-28 10:13:07'),
(2, 'Bharathiyar University', 1, '2020-04-17 06:23:21', '2020-04-28 04:04:13');

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `type` enum('percentage','amount') DEFAULT 'percentage',
  `value` float DEFAULT NULL,
  `active` int(5) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `code`, `type`, `value`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 'Test100', 'percentage', 50, 1, '2020-04-25 11:56:22', '2020-04-27 08:22:10'),
(2, 'Test200', 'amount', 1500, 1, '2020-04-25 12:00:31', '2020-04-25 12:00:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colleges`
--
ALTER TABLE `colleges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_supports`
--
ALTER TABLE `customer_supports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deep_dives`
--
ALTER TABLE `deep_dives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `degrees`
--
ALTER TABLE `degrees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_transactions`
--
ALTER TABLE `payment_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchased_courses`
--
ALTER TABLE `purchased_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_preps`
--
ALTER TABLE `quick_preps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_prep_deep_dive_links`
--
ALTER TABLE `quick_prep_deep_dive_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_prep_indices`
--
ALTER TABLE `quick_prep_indices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_prep_resources`
--
ALTER TABLE `quick_prep_resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register_count_details`
--
ALTER TABLE `register_count_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semesters`
--
ALTER TABLE `semesters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `colleges`
--
ALTER TABLE `colleges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer_supports`
--
ALTER TABLE `customer_supports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `deep_dives`
--
ALTER TABLE `deep_dives`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `degrees`
--
ALTER TABLE `degrees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `payment_transactions`
--
ALTER TABLE `payment_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchased_courses`
--
ALTER TABLE `purchased_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quick_preps`
--
ALTER TABLE `quick_preps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `quick_prep_deep_dive_links`
--
ALTER TABLE `quick_prep_deep_dive_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `quick_prep_indices`
--
ALTER TABLE `quick_prep_indices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `quick_prep_resources`
--
ALTER TABLE `quick_prep_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `register_count_details`
--
ALTER TABLE `register_count_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `semesters`
--
ALTER TABLE `semesters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

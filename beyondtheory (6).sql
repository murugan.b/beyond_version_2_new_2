-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 20, 2020 at 10:48 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beyondtheory`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `super_admin` tinyint(1) DEFAULT 0,
  `delete_status` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `full_name`, `phone`, `email`, `password`, `super_admin`, `delete_status`, `active`, `createdAt`, `updatedAt`) VALUES
(1, NULL, NULL, 'admin@gmail.com', 'sha1$6e2faf46$1$70707522fb88da9bd23c0ac5c7a2411c1f025ecb', 0, 0, 1, '0000-00-00 00:00:00', '2020-04-18 11:32:18');

-- --------------------------------------------------------

--
-- Table structure for table `colleges`
--

CREATE TABLE `colleges` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `university_id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `colleges`
--

INSERT INTO `colleges` (`id`, `name`, `university_id`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 'PSYEC', 1, 1, '0000-00-00 00:00:00', '2020-04-17 07:00:00'),
(2, 'Chennai Anna', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'SIT', 2, 1, '2020-04-17 06:56:29', '2020-04-17 06:56:29'),
(4, 'Kamarajar Engineering College', 1, 1, '2020-04-17 06:56:48', '2020-04-17 07:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `university_id` int(11) DEFAULT NULL,
  `degree_id` int(11) DEFAULT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `api_token` varchar(60) DEFAULT NULL,
  `delete_status` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `deep_dives`
--

CREATE TABLE `deep_dives` (
  `id` int(11) NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `content_title` varchar(255) DEFAULT NULL,
  `listing_order` int(11) DEFAULT 1,
  `locked` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `deep_dives`
--

INSERT INTO `deep_dives` (`id`, `module_id`, `content_title`, `listing_order`, `locked`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'sdfsdfsdfsdfsdfsdf', 1, 0, 1, '0000-00-00 00:00:00', '2020-04-20 07:34:54'),
(2, 3, 'test murugan content ', 1, 0, 1, '2020-04-20 07:01:58', '2020-04-20 07:35:01'),
(3, 4, 'Test 2 Murugannnnnnn', 1, 0, 1, '2020-04-20 07:28:26', '2020-04-20 07:34:57');

-- --------------------------------------------------------

--
-- Table structure for table `degrees`
--

CREATE TABLE `degrees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `college_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `university_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `degrees`
--

INSERT INTO `degrees` (`id`, `name`, `college_id`, `active`, `createdAt`, `updatedAt`, `university_id`) VALUES
(1, 'BE-CSE', 1, 1, '2020-04-17 11:58:37', '2020-04-17 11:58:37', 0),
(2, 'BE-ECE', 2, 1, '2020-04-17 11:58:57', '2020-04-20 03:59:39', 0),
(3, 'BE-IT', 3, 1, '2020-04-17 11:59:19', '2020-04-17 11:59:19', 0),
(4, 'BE-MECH', 1, 1, '2020-04-17 11:59:39', '2020-04-17 11:59:39', 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `listing_order` int(11) DEFAULT 1,
  `subject_id` int(11) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `listing_order`, `subject_id`, `locked`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 'Test1', 1, 1, 0, 1, '2020-04-15 10:47:54', '2020-04-17 05:15:21'),
(2, 'test2', 1, 2, 0, 1, '2020-04-15 10:48:03', '2020-04-17 05:15:23'),
(3, 'Test Murugan', 1, 1, 0, 1, '2020-04-15 11:11:14', '2020-04-17 05:13:19'),
(4, 'Murugan 2 Test', 1, 2, 0, 1, '2020-04-17 05:13:32', '2020-04-17 05:15:24');

-- --------------------------------------------------------

--
-- Table structure for table `quick_preps`
--

CREATE TABLE `quick_preps` (
  `id` int(11) NOT NULL,
  `quick_prep_index_id` int(11) DEFAULT NULL,
  `question_title` varchar(255) DEFAULT NULL,
  `question_description` varchar(255) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quick_preps`
--

INSERT INTO `quick_preps` (`id`, `quick_prep_index_id`, `question_title`, `question_description`, `locked`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'qtitle', 'qdescription', 0, 1, '2020-04-20 08:35:03', '2020-04-20 08:35:03');

-- --------------------------------------------------------

--
-- Table structure for table `quick_prep_deep_dive_links`
--

CREATE TABLE `quick_prep_deep_dive_links` (
  `id` int(11) NOT NULL,
  `quick_prep_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `deep_dive_id` int(11) DEFAULT NULL,
  `deep_dive_resource_id` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `quick_prep_indices`
--

CREATE TABLE `quick_prep_indices` (
  `id` int(11) NOT NULL,
  `exam_year` varchar(255) DEFAULT NULL,
  `exam_month` varchar(255) DEFAULT NULL,
  `listing_order` int(11) DEFAULT 1,
  `subject_id` int(11) DEFAULT NULL,
  `download_link` varchar(255) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quick_prep_indices`
--

INSERT INTO `quick_prep_indices` (`id`, `exam_year`, `exam_month`, `listing_order`, `subject_id`, `download_link`, `locked`, `active`, `createdAt`, `updatedAt`) VALUES
(1, '2019', 'September', 1, 1, 'google.com', 0, 1, '0000-00-00 00:00:00', '2020-04-20 06:01:11'),
(2, '2019', 'Test Month', 1, 2, 'test.com', 0, 1, '2020-04-20 03:53:32', '2020-04-20 06:01:09');

-- --------------------------------------------------------

--
-- Table structure for table `quick_prep_resources`
--

CREATE TABLE `quick_prep_resources` (
  `id` int(11) NOT NULL,
  `question_sub_title` varchar(255) DEFAULT NULL,
  `quick_prep_id` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `type` enum('video','doc') DEFAULT 'video',
  `locked` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `semesters`
--

CREATE TABLE `semesters` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `degree_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `semester_id`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 'Murugan', 1, 1, '2020-04-15 10:44:03', '2020-04-17 12:13:14'),
(2, 'Murugan2', 2, 1, '2020-04-15 10:44:10', '2020-04-15 10:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE `universities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `universities`
--

INSERT INTO `universities` (`id`, `name`, `active`, `createdAt`, `updatedAt`) VALUES
(1, 'Anna University', 1, '2020-04-17 06:23:07', '2020-04-17 06:23:50'),
(2, 'Bharathiyar University', 1, '2020-04-17 06:23:21', '2020-04-17 06:23:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colleges`
--
ALTER TABLE `colleges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deep_dives`
--
ALTER TABLE `deep_dives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `degrees`
--
ALTER TABLE `degrees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_preps`
--
ALTER TABLE `quick_preps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_prep_deep_dive_links`
--
ALTER TABLE `quick_prep_deep_dive_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_prep_indices`
--
ALTER TABLE `quick_prep_indices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_prep_resources`
--
ALTER TABLE `quick_prep_resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semesters`
--
ALTER TABLE `semesters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `colleges`
--
ALTER TABLE `colleges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deep_dives`
--
ALTER TABLE `deep_dives`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `degrees`
--
ALTER TABLE `degrees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `quick_preps`
--
ALTER TABLE `quick_preps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quick_prep_deep_dive_links`
--
ALTER TABLE `quick_prep_deep_dive_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quick_prep_indices`
--
ALTER TABLE `quick_prep_indices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `quick_prep_resources`
--
ALTER TABLE `quick_prep_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `semesters`
--
ALTER TABLE `semesters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
